class AssetsRoutes {
  // Server URL
  static const String servers = "assets/values/servers.json";

  static const String account = "assets/images/account.png";
  static const String ceo = "assets/images/ceo.png";
  static const String coach = "assets/images/coach.png";
  static const String company = "assets/images/company.png";
  static const String home = "assets/images/home.png";
  static const String img1 = "assets/images/img1.png";
  static const String img2 = "assets/images/img2.png";
  static const String img3 = "assets/images/img3.png";
  static const String img4 = "assets/images/img4.png";
  static const String img5 = "assets/images/img5.png";
  static const String img6 = "assets/images/img6.png";
  static const String img7 = "assets/images/img7.png";
  static const String logo = "assets/images/logo.png";
  static const String manager = "assets/images/manager.png";
  static const String more = "assets/images/more.png";
  static const String player = "assets/images/player.png";
  static const String rules = "assets/images/rules.png";
  static const String services = "assets/images/services.png";
  static const String splash = "assets/images/splash.png";
  static const String wkf_logo = "assets/images/wkf_logo.png";
  static const String img_icon = "assets/images/img_icon.png";
}
