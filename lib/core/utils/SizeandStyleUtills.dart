import 'package:flutter/material.dart';

class SizeandStyleUtills {
  factory SizeandStyleUtills() {
    return _singleton;
  }

  static final SizeandStyleUtills _singleton = SizeandStyleUtills._internal();

  SizeandStyleUtills._internal();

//region Screen Size and Proportional according to device
  double? _screenHeight;
  double? _screenWidth;

  double? get screenHeight => _screenHeight;

  double? get screenWidth => _screenWidth;
  final double? _refrenceScreenHeight = 683.4285714285714;
  final double? _refrenceScreenWidth = 411.42857142857144;

  void updateScreenDimesion({double? width, double? height}) {
    _screenWidth = (width != null) ? width : _screenWidth;
    _screenHeight = (height != null) ? height : _screenHeight;
  }

  double getProportionalHeight({double? height}) {
    if (_screenHeight == null) return height!;
    return _screenHeight! * height! / _refrenceScreenHeight!;
  }

  double getProportionalWidth({double? width}) {
    if (_screenWidth == null) return width!;
    var w = _screenWidth! * width! / _refrenceScreenWidth!;
    return w.ceilToDouble();
  }

//endregion
//region TextStyle
  TextStyle getTextStyleRegular(
      {FontWeight? fontWeight,
        String fontName = 'Almarai',
        int fontsize = 16,
        Color? color,
        bool isChangeAccordingToDeviceSize = true,
        double? characterSpacing,
        double? lineSpacing}) {
    double finalFontsize = fontsize.toDouble();
    if (isChangeAccordingToDeviceSize && this._screenWidth != null) {
      finalFontsize = (finalFontsize * _screenWidth!) / _refrenceScreenWidth!;
    }
    if (fontWeight != null) {
      return TextStyle(
          fontSize: finalFontsize,
          fontFamily: 'Almarai',
          color: color,
          fontWeight: fontWeight);
    } else if (characterSpacing != null) {
      return TextStyle(
          fontSize: finalFontsize,
          fontFamily: 'Almarai',
          color: color,
          letterSpacing: characterSpacing);
    } else if (lineSpacing != null) {
      return TextStyle(
          fontSize: finalFontsize,
          fontFamily: 'Almarai',
          color: color,
          height: lineSpacing);
    }
    return TextStyle(
        fontSize: finalFontsize, fontFamily: 'Almarai', color: color);
  }
}
