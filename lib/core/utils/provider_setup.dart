// provider_setup.dart
import 'package:UAE_KF/auth/authentication_service.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = [
  ...independentServices,
  // ...dependentServices,
];

List<SingleChildWidget> independentServices = [
  Provider<AuthenticationService>(create: (_) => AuthenticationService()),
];

// List<SingleChildWidget> dependentServices = [
// ];
