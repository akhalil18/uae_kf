class ApiRoutes {
  static const String login = "login";
  static const String register = "register";
  static const String checkUserEmail = "checkUserEmail";
  static const String updateUserProfile = "updateUserProfile";
  static const String forgetPassword = "forgetPassword";
  static const String resendResetCode = "resendResetCode";
  static const String resetPassword = "resetPassword";
  static const String checkResetCode = "checkResetCode";

  static const String homePage = "homePage";
  static const String getFederationActivities = "getFederationActivities";
  static const String getInternationalAgenda = "getInternationalAgenda";
  static const String getAllCourses = "getAllCourses";
  static const String contactUs = "contactUs";
  static const String setUserComplaint = "setUserComplaint";
  static const String getAllChampionships = "getAllChampionships";
  static const String getAboutUsPage = "getAboutUsPage";
  static const String setUserChampionship = "setUserChampionship";
  static const String setUserTraining = "setUserTraining";
  static const String setUserVolunteering = "setUserVolunteering";
  static const String checkActiveCode = "checkActiveCode";
}
