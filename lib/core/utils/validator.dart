import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

mixin Validator {
  // String phoneValidator(String value, BuildContext context) {
  //   if (value == null || value.isEmpty) {
  //     return AppLocalizations.of(context).translate("phone_validator");
  //   }
  //   return null;
  // }

  String? namelValidator(String value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context).translate('name_validator');
    } else if (value.length < 2) {
      return AppLocalizations.of(context).translate('name_short');
    }
    return null;
  }

  String? emailValidator(String value, BuildContext context) {
    Pattern pattern = r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
    RegExp regExp = RegExp(pattern.toString());
    if (!regExp.hasMatch(value)) {
      return AppLocalizations.of(context).translate('email_validator');
    }
    return null;
  }

  String? passwordValidator(String value, BuildContext context) {
    if (value.length < 6) {
      return AppLocalizations.of(context).translate('password_validator');
    }
    return null;
  }

  String? mobileValidator(String value, BuildContext context) {
    if (value.isEmpty) {
      return AppLocalizations.of(context).translate('phone_validator');
    }
    return null;
  }

  String? userNameValidator(String value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return 'Username is empty';
    } else if (value.length < 2) {
      return 'Too short username';
    }
    return null;
  }

  String? identifyValidator(String value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context).translate('identify_validator');
    }
    return null;
  }

  String? messageValidator(String value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context).translate('message_validator');
    }
    return null;
  }

  String? fieldValidator(String value, BuildContext context) {
    if (value == null || value.isEmpty) {
      return AppLocalizations.of(context).translate('field_validator');
    }
    return null;
  }
}
