import 'package:UAE_KF/core/preference/preference.dart';

class Header {
  static Map<String, String> get userAuth => {
        "Content-Type": "application/json",
        'Authorization': 'Bearer ${Preference.getString(PrefKeys.token)}',
        'lang': '${Preference.getString(PrefKeys.languageCode) ?? 'ar'}'
      };

  static Map<String, String> get noAuth => {
        "Content-Type": "application/json",
        'lang': '${Preference.getString(PrefKeys.languageCode) ?? 'ar'}'
      };
}
