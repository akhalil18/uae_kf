import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class DropItem {
  final String? name;
  final String? value;

  DropItem({this.name, this.value});
}

// get Category
List<DropItem> getCatrgory(BuildContext context) {
  var locale = AppLocalizations.of(context);
  return [
    DropItem(name: locale!.translate('player'), value: 'player'),
    DropItem(name: locale!.translate('coach'), value: 'coach'),
    DropItem(name: locale!.translate('referee'), value: 'referee'),
    DropItem(name: locale!.translate('manager'), value: 'manger'),
    DropItem(name: locale!.translate('management-board-member'), value: 'mbd'),
    DropItem(
        name: locale!.translate('strategic-company'),
        value: 'strategic_companies')
  ];
}

// get degree of bill list.
List<DropItem> getBeltDegree(BuildContext context) {
  var locale = AppLocalizations.of(context);
  return [
    DropItem(name: locale!.translate('yellow'), value: 'yellow'),
    DropItem(name: locale!.translate('orange'), value: 'orange'),
    DropItem(name: locale!.translate('green'), value: 'green'),
    DropItem(name: locale!.translate('blue'), value: 'blue'),
    DropItem(name: locale!.translate('brown'), value: 'brown'),
    DropItem(name: locale!.translate('black'), value: 'black'),
  ];
}

// Get clubs list
List<DropItem> getClubs(BuildContext context) {
  var locale = AppLocalizations.of(context);
  return [
    DropItem(
        name: locale!.translate('sharjah-self-defense'),
        value: 'Sharjah Self-Defense Sports'),
    DropItem(
        name: locale!.translate('sharjah-individual-games'),
        value: 'Sharjah Individual Games'),
    DropItem(name: locale!.translate('shabab-alahli'), value: 'Shabab Alahli'),
    DropItem(name: locale!.translate('ajman'), value: 'Ajman'),
    DropItem(name: locale!.translate('al-nasr'), value: 'Al-Nasr'),
    DropItem(name: locale!.translate('ittihad-kalba'), value: 'Ittihad Kalba'),
    DropItem(name: locale!.translate('dhaid'), value: 'Dhaid'),
  ];
}

// Get weight
List<String> getWeight(String category, String gender) {
  switch (category) {
    case 'cubs_and_girls':
      if (gender == 'male' || gender == 'ذكر') {
        return ['-30', '-35', '-40', '+40'];
      } else {
        return ['-29', '-34', '-39', '+39'];
      }
      break;

    case 'budding':
      if (gender == 'male' || gender == 'ذكر') {
        return ['-52', '-57', '-63', '-70', '+70'];
      } else {
        return ['-47', '-54', '+54'];
      }
      break;

    case 'youth':
      if (gender == 'male' || gender == 'ذكر') {
        return ['-55', '-61', '-68', '-76', '+76'];
      } else {
        return ['48', '-53', '-59', '+59'];
      }
      break;

    case 'men_and_women':
      if (gender == 'male' || gender == 'ذكر') {
        return ['-60', '-67', '-75', '-84', '+84'];
      } else {
        return ['-50', '-55', '-61', '-68', '+68'];
      }
      break;

    default:
      {
        return [];
      }
  }
}
