import 'package:flutter/material.dart';

class GoTo {
  // go to screen and pass data to constractor
  static void screen(BuildContext context, dynamic screen) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => screen),
    );
  }

  // go to screen and remove until and pass data to constractor
  static void screenAndRemoveUntil(BuildContext context, dynamic screen) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => screen),
        (Route<dynamic> route) => false);
  }

  // go to screen and replace old and pass data to constractor
  static void screenAndReplacement(BuildContext context, dynamic screen) {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => screen),
    );
  }
}
