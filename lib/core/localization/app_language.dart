import 'package:flutter/material.dart';

import '../preference/preference.dart';

class AppLanguage extends ChangeNotifier {
  Locale _appLocale = Locale('en', '');

  Locale get appLocal => _appLocale ?? Locale("en");

  Future<void> fetchLocale() async {
    if (Preference.getString(PrefKeys.languageCode) == null) {
      _appLocale = Locale('en');
    } else {
      _appLocale = Locale(Preference.getString(PrefKeys.languageCode)!);
    }
    notifyListeners();
  }

  Future<void> changeLanguage(Locale type, {bool firstChange = false}) async {
    if (_appLocale == type && !firstChange) {
      return;
    }
    if (type == Locale("ar")) {
      _appLocale = Locale("ar");
      await Preference.setString(PrefKeys.languageCode, 'ar');
      await Preference.setString('countryCode', '');
    } else if (type == Locale("en")) {
      _appLocale = Locale("en");
      await Preference.setString(PrefKeys.languageCode, 'en');
    }
    notifyListeners();
  }
}
