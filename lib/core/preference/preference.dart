import 'package:shared_preferences/shared_preferences.dart';

class PrefKeys {
  static const String token = 'token';
  static const String userData = 'userData';
  static const String userLogged = 'userLogged';
  static const String languageCode = 'language_code';
}

class Preference {
  static SharedPreferences? sp;

  static Future<void> init() async {
    if (sp == null) sp = await SharedPreferences.getInstance();
  }

  static String? getString(String key) {
    try {
      return sp!.getString(key);
    } catch (e) {
      return null;
    }
  }

  static int? getInt(String key) {
    try {
      return sp!.getInt(key);
    } catch (e) {
      return null;
    }
  }

  static bool? getBool(String key) {
    try {
      return sp!.getBool(key);
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<bool?> setString(String key, String value) async {
    final sp = await SharedPreferences.getInstance();
    try {
      return sp.setString(key, value);
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<bool?> setInt(String key, int value) async {
    try {
      return sp!.setInt(key, value);
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<bool?> setBool(String key, bool value) async {
    try {
      print(key);
      return await sp!.setBool(key, value);
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<bool?> remove(String key) async {
    try {
      return await sp!.remove(key);
    } catch (e) {
      return null;
    }
  }

  static Future<bool?> clear() async {
    try {
      return await sp!.clear();
    } catch (e) {
      return null;
    }
  }
}
