// import 'package:flutter/material.dart';

// import '../screens/auth_pages/change_password_screen.dart';
// import '../screens/auth_pages/code_verify_screen.dart';
// import '../screens/auth_pages/reset_password_screen.dart';
// import '../screens/auth_pages/sign_up_screen.dart';
// import '../screens/main_pages/main_screen.dart';
// import '../screens/splash_screen/splash_screen.dart';
// import 'route_constants.dart';

// class RouteGenerator {
//   static Route<dynamic> generateRoute(RouteSettings settings) {
//     // final args = settings.arguments;

//     switch (settings.name) {
//       case MainScreenRoute:
//         return MaterialPageRoute(builder: (_) => MainScreen());

//       case SplashScreenRoute:
//         return MaterialPageRoute(builder: (_) => SplashScreen());

//       // case LoginTypeScreenRoute:
//       //   return MaterialPageRoute(builder: (_) => LoginTypeScreen());

//       // case LoginScreenRoute:
//       //   return MaterialPageRoute(builder: (_) => LoginScreen());

//       case ResetPasswordScreenRoute:
//         return MaterialPageRoute(builder: (_) => ResetPasswordScreen());

//       case CodeVerifyScreenRoute:
//         return MaterialPageRoute(builder: (_) => CodeVerifyScreen());

//       case ChangePasswordScreenRoute:
//         return MaterialPageRoute(builder: (_) => ChangePasswordScreen());

//       case StringSignUpScreenRoute:
//         return MaterialPageRoute(builder: (_) => SignUpScreen());

//       default:
//         return MaterialPageRoute(builder: (_) => SplashScreen());
//     }
//   }
// }
