import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../core/utils/AssetsRoutes.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/white_button.dart';
import '../auth_pages/login_screen.dart';
import '../main_pages/main_screen.dart';

class LoginTypeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: ScreenUtil.screenWidthDp,
            height: ScreenUtil.screenHeightDp,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/img1.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: ScreenUtil.portrait!
                ? ScreenUtil.screenHeightDp! * 0.33
                : ScreenUtil.screenHeightDp! * 0.1,
            child: Image.asset(
              AssetsRoutes.logo,
              width: 150,
              height: 150,
            ),
          ),
          Positioned(
            bottom: ScreenUtil.portrait!
                ? ScreenUtil.screenHeightDp! * 0.1
                : ScreenUtil.screenHeightDp! * 0.06,
            left: 0,
            right: 0,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil.screenWidthDp! * .08),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ButtonWithGradient(
                    title: AppLocalizations.of(context).translate('sign-in'),
                    onTap: () async {
                      GoTo.screen(context, LoginScreen());
                    },
                  ),
                  SizedBox(height: 15),
                  WhiteButton(
                    color: Colors.white,
                    title: AppLocalizations.of(context)
                        .translate('login-as-visitor'),
                    onTab: () {
                      // Navigator.of(context).pushNamed(MainScreenRoute);
                      GoTo.screenAndRemoveUntil(context, MainScreen());
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
