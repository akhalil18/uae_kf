import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../core/localization/app_language.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../widgets/app_header.dart';
import '../../widgets/language_buttons.dart';
import '../../widgets/title_with_back_button.dart';

class ChangeLanguageScreen extends StatefulWidget {
  @override
  _ChangeLanguageScreenState createState() => _ChangeLanguageScreenState();
}

class _ChangeLanguageScreenState extends State<ChangeLanguageScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(AppLocalizations.of(context)
                          !.translate('change-language')),
                      KLargeSizedBoxHeight,
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Consumer<AppLanguage>(
                          builder: (context, value, child) {
                            var isArabic = value.appLocal == Locale("ar");
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              textDirection: TextDirection.ltr,
                              children: [
                                Expanded(
                                    child: LanguageButton(
                                  title: 'English',
                                  onTab: () {
                                    changeLocale(context, Locale("en"));
                                  },
                                  color: isArabic
                                      ? Colors.transparent
                                      : AppColors.accentColor,
                                )),
                                SizedBox(width: 20),
                                Expanded(
                                    child: LanguageButton(
                                  title: 'عربى',
                                  onTab: () {
                                    changeLocale(context, Locale("ar"));
                                  },
                                  color: isArabic
                                      ? AppColors.accentColor
                                      : Colors.transparent,
                                )),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changeLocale(BuildContext context, Locale locale) {
    context.read<AppLanguage>().changeLanguage(locale);
  }
}
