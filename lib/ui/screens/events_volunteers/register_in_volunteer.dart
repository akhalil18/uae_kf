import 'package:flutter/material.dart';

import '../../../controller/volunteeringRegisterController.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/app_header.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_drop_down_button.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/title_with_back_button.dart';

class RegisterInVolunterr extends StatefulWidget {
  final String? type;
  final String? title;
  const RegisterInVolunterr({this.type, this.title});

  @override
  _RegisterInVolunterrState createState() => _RegisterInVolunterrState();
}

class _RegisterInVolunterrState extends State<RegisterInVolunterr>
    with Validator {
  var _formKey = GlobalKey<FormState>();

  var _name = TextEditingController();
  var _phone = TextEditingController();
  var _address = TextEditingController();
  var _hours = TextEditingController();
  String? _category;

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(widget.title!),

                      // body
                      buildBody(context),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // name
            Text(applocale!.translate('name'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _name,
              hint: applocale!.translate('your-name'),
              validator: (value) => namelValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // type
            Text(applocale!.translate('category'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: applocale!.translate('category'),
              items: [
                DropdownMenuItem(
                  child: Text(applocale!.translate('player')),
                  value: 'player',
                ),
                DropdownMenuItem(
                  child: Text(applocale!.translate('coach')),
                  value: 'coach',
                ),
                DropdownMenuItem(
                  child: Text(applocale!.translate('referee')),
                  value: 'referee',
                ),
              ],
              value: _category,
              onChange: (value) {
                setState(() {
                  _category = value;
                });
              },
            ),

            KMediumSizedBoxHeight,

            // hours
            Text(applocale!.translate('volunteering-hours'),
                style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _hours,
              hint: applocale!.translate('volunteering-hours'),
              validator: (value) => fieldValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // Phone number
            Text(applocale!.translate('phone-number'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            Container(
              child: Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: 45,
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.black54),
                      ),
                      child: Text('‎+971'),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    flex: 6,
                    child: CustomTextFormField(
                      controller: _phone,
                      hint: AppLocalizations.of(context)
                          .translate('phone-number'),
                      keyboardType: TextInputType.phone,
                      // validator: (value) => mobileValidator(value, context),
                    ),
                  )
                ],
              ),
            ),

            KMediumSizedBoxHeight,

            // address
            Text(applocale!.translate('address'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _address,
              hint: applocale!.translate('address'),
              validator: (value) => fieldValidator(value!, context),
            ),

            KLargeSizedBoxHeight,

            // send
            ButtonWithGradient(
              busy: isLoading,
              onTap: () {
                _send(
                    type: widget.type!,
                    context: context,
                    address: _address.text,
                    category: _category!,
                    hours: _hours.text,
                    phone: _phone.text,
                    name: _name.text);
              },
              title: applocale!.translate('send'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _send(
      {String? type,
      String? name,
      String? phone,
      String? hours,
      String? category,
      String? address,
      BuildContext? context}) async {
    var appLocale = AppLocalizations.of(context!);

    var isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    setState(() {
      isLoading = true;
    });

    final statue = await VolunteeringRegisterController()
        .registerInVolunteering(
            type: type,
            address: address,
            category: category,
            hours: hours,
            name: name,
            phone: phone);
    if (statue) {
      UI.dialog(
        context: context,
        title: applocale!.translate('success'),
        titleColor: Colors.green,
        msg: applocale!.translate('successfully-registered'),
        child: Icon(Icons.check_circle_outline, color: Colors.green, size: 35),
      );
      clearData();
    } else {
      UI.dialog(
        context: context,
        title: applocale!.translate('failed'),
        msg: applocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    }
    setState(() {
      isLoading = false;
    });
  }

  void clearData() {
    setState(() {
      _name.clear();
      _phone.clear();
      _address.clear();
      _hours.clear();
      _category = null;
    });
  }
}
