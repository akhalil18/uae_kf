import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../auth/authentication_service.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../widgets/app_header.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/circle_with_action.dart';
import '../../widgets/title_with_back_button.dart';
import '../../widgets/white_button.dart';
import '../auth_pages/login_screen.dart';
import '../auth_pages/sign_up_screen.dart';
import 'register_in_volunteer.dart';

class EventsVolunteers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    var appLocale = AppLocalizations.of(context);
    var auth = Provider.of<AuthenticationService>(context, listen: false);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(
                          applocale!.translate('events-volunteers')),
                      KMediumSizedBoxHeight,

                      // body
                      !auth.userLoged
                          ? noAuthBody(context)
                          : Wrap(
                              crossAxisAlignment: WrapCrossAlignment.start,
                              alignment: WrapAlignment.center,
                              children: [
                                CircleWithAction(
                                  size: 85,
                                  title: applocale!.translate('local'),
                                  onTap: () {
                                    GoTo.screen(
                                      context,
                                      RegisterInVolunterr(
                                          title: applocale!.translate('local'),
                                          type: 'local'),
                                    );
                                  },
                                ),
                                CircleWithAction(
                                  size: 85,
                                  title: applocale!.translate('arabic'),
                                  color: Colors.white,
                                  onTap: () {
                                    GoTo.screen(
                                      context,
                                      RegisterInVolunterr(
                                          title: applocale!.translate('arabic'),
                                          type: 'arabic'),
                                    );
                                  },
                                ),
                                CircleWithAction(
                                  size: 85,
                                  title: applocale!.translate('asian'),
                                  onTap: () {
                                    GoTo.screen(
                                      context,
                                      RegisterInVolunterr(
                                          title: applocale!.translate('asian'),
                                          type: 'asian'),
                                    );
                                  },
                                ),
                                CircleWithAction(
                                  size: 85,
                                  title: applocale!.translate('international'),
                                  onTap: () {
                                    GoTo.screen(
                                      context,
                                      RegisterInVolunterr(
                                          title: appLocale
                                              .translate('international'),
                                          type: 'international'),
                                    );
                                  },
                                ),
                              ],
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container noAuthBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Container(
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              applocale!.translate('login-messgae'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: Colors.white),
            ),
            KLargeSizedBoxHeight,
            ButtonWithGradient(
              onTap: () {
                GoTo.screen(context, LoginScreen());
              },
              title: applocale!.translate('sign-in'),
            ),
            KLargeSizedBoxHeight,
            WhiteButton(
              title: applocale!.translate('new-account'),
              onTab: () {
                GoTo.screen(context, SignUpScreen());
              },
            ),
          ],
        ),
      ),
    );
  }
}
