import 'dart:async';

import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

import '../../../auth/authentication_service.dart';
import '../../../controller/StartUpController.dart';
import '../../../core/connectivity/connection_statue.dart';
import '../../../core/localization/app_language.dart';
import '../../../core/preference/preference.dart';
import '../../../core/utils/AssetsRoutes.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../widgets/floating_widget.dart';
import '../../widgets/language_buttons.dart';
import '../login_type_screen/login_type_Screen.dart';
import '../main_pages/main_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  StreamSubscription? connectionChangeStream;
  ConnectionStatusSingleton? connectionStatus;
  AuthenticationService? auth;

  bool isOnline = true;
  bool isNavigating = false;

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      // get Server url
      StartUpController().initServerUrl();

      await Provider.of<AppLanguage>(context, listen: false).fetchLocale();
      auth = Provider.of<AuthenticationService>(context, listen: false);

      connectionStatus = ConnectionStatusSingleton.getInstance();

      // listen to change in connection
      connectionChangeStream =
          connectionStatus!.connectionChange.listen(connectionChanged);

      navigate();
    });
  }

  Future<void> navigate() async {
    var savedLanguage = Preference.getString(PrefKeys.languageCode);
    // if navigating or no enternet return
    if (isNavigating || !isOnline || savedLanguage == null) {
      return;
    }

    setState(() {
      isNavigating = true;
    });

    await Future.delayed(Duration(seconds: 2));

    setState(() {
      isNavigating = false;
    });

    if (isOnline) {
      goToSecondPage();
    }
  }

  void connectionChanged(dynamic hasConnection) {
    bool lastCheck = isOnline;
    // Check if connection statue changed
    if (lastCheck != hasConnection) {
      print(hasConnection);
      setState(() {
        isOnline = hasConnection;
      });
      navigate();
    }
  }

  void goToSecondPage() {
    try {
      if (auth!.userLoged) {
        GoTo.screenAndReplacement(context, MainScreen());
      } else {
        GoTo.screenAndReplacement(context, LoginTypeScreen());
      }
    } catch (e) {
      print(e);
      GoTo.screenAndReplacement(context, LoginTypeScreen());
    }
  }

  @override
  void dispose() {
    connectionChangeStream!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    return Scaffold(
      body: AnimatedCrossFade(
        layoutBuilder:
            (Widget topChild, Key topKey, Widget bottomChild, Key bottomKey) {
          return Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned(
                child: bottomChild,
                key: bottomKey,
              ),
              Positioned(child: topChild, key: topKey),
            ],
          );
        },
        firstChild: buildLoadedScreen(),
        secondChild: offlineWidget(context),
        crossFadeState:
            isOnline ? CrossFadeState.showFirst : CrossFadeState.showSecond,
        duration: Duration(milliseconds: 600),
      ),
    );
  }

  Widget buildLoadedScreen() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: ScreenUtil.screenWidthDp,
          height: ScreenUtil.screenHeightDp,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/splash.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: ScreenUtil.screenWidthDp,
            height: ScreenUtil.screenHeightDp! / 3,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xff1e284b),
                  Color(0xffd63c455f),
                  Color(0xff005f6683),
                ],
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter,
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                AssetsRoutes.logo,
                width: 150,
              ),

              // Languages Buttons
              if (Preference.getString(PrefKeys.languageCode) == null)
                // If no language saved ...  show language buttons
                ...[
                SizedBox(
                    height: ScreenUtil.portrait!
                        ? ScreenUtil.screenHeightDp! * 0.3
                        : ScreenUtil.screenHeightDp! * 0.075),
                Text(
                  AppLocalizations.of(context)!.translate('choose-language'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 19,
                  ),
                ),

                KLargeSizedBoxHeight,

                // change language button
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Consumer<AppLanguage>(
                    builder: (context, value, child) {
                      var isArabic = value.appLocal == Locale("ar");
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        textDirection: TextDirection.ltr,
                        children: [
                          Expanded(
                              child: LanguageButton(
                            title: 'English',
                            onTab: () {
                              changeLocale(context, Locale("en"));
                            },
                            color: isArabic
                                ? Colors.transparent
                                : AppColors.accentColor,
                          )),
                          SizedBox(width: 20),
                          Expanded(
                              child: LanguageButton(
                            title: 'عربى',
                            onTab: () {
                              changeLocale(context, Locale("ar"));
                            },
                            color: isArabic
                                ? AppColors.accentColor
                                : Colors.transparent,
                          )),
                        ],
                      );
                    },
                  ),
                ),
              ]
            ],
          ),
        ),
      ],
    );
  }

  Future<void> changeLocale(BuildContext context, Locale locale) async {
    await context.read<AppLanguage>().changeLanguage(locale, firstChange: true);
    goToSecondPage();
  }

  Widget offlineWidget(BuildContext context) {
    var locale = AppLocalizations.of(context);
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FloatingWidget(
            child:
                Icon(Icons.portable_wifi_off, size: 80, color: Colors.red[800]),
          ),
          Text(
            locale!.translate('network-connection-error'),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey[800], fontSize: 20),
          ),
          SizedBox(height: 10),
          RaisedButton(
            child: Text(locale!.translate('try-again')),
            onPressed: () {
              connectionStatus!.checkConnection();
            },
          ),
        ],
      ),
    );
  }
}
