import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/ui/screens/service_tab_details/make_complaint/make_complaint.dart';
import 'package:UAE_KF/ui/screens/service_tab_details/remote_courses/remote_courses.dart';
import 'package:flutter/material.dart';

import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../widgets/circle_with_action.dart';
import '../../widgets/app_header.dart';
import '../../widgets/title_with_back_button.dart';
import 'champ_register/champ_registration.dart';
import 'trainig_register/trainig_registration.dart';

class ServiceTabDetails extends StatelessWidget {
  final String? title;
  const ServiceTabDetails({this.title});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(title!),
                      KMediumSizedBoxHeight,

                      // body
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        alignment: WrapAlignment.center,
                        children: [
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('registration-at-championship'),
                            onTap: () {
                              GoTo.screen(context, ChampRegistration());
                            },
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('my-certificates'),
                            color: Colors.white,
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('previous-posts'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('exam'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('certificate-whom-it-may-concern'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title:
                                appLocale!.translate('complaint-or-grievance'),
                            onTap: () {
                              GoTo.screen(context, MakeComplaint());
                            },
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('Training-Course'),
                            onTap: () {
                              GoTo.screen(context, TrainigRegistration());
                            },
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('Refereeing-Course'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('remote-courses'),
                            onTap: () {
                              GoTo.screen(context, RemoteCourses());
                            },
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('Player-Card'),
                            onTap: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
