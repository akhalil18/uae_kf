import 'package:UAE_KF/controller/trainingRegisterController.dart';
import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/core/utils/validator.dart';
import 'package:UAE_KF/ui/shared/ui.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/dropDownLists.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/styles/text_style.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../../widgets/custom_drop_down_button.dart';
import '../../../widgets/custom_text_form_field.dart';
import '../../../widgets/title_with_back_button.dart';

class TrainigRegistration extends StatefulWidget {
  @override
  _TrainigRegistrationState createState() => _TrainigRegistrationState();
}

class _TrainigRegistrationState extends State<TrainigRegistration>
    with Validator {
  var _formKey = GlobalKey<FormState>();
  var _name = TextEditingController();
  String? _club;
  String? _beltDegree;

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    var applocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(
                          applocale!.translate('training-registration')),
                      KMediumSizedBoxHeight,

                      // body
                      buildBody(context),

                      // Submit
                      KLargeSizedBoxHeight,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: ButtonWithGradient(
                          busy: _isLoading,
                          onTap: () {
                            submit(
                                agency: _club!,
                                name: _name.text,
                                belt: _beltDegree!);
                          },
                          title: applocale!.translate('subscribe'),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildBody(BuildContext context) {
    var applocale = AppLocalizations.of(context);

    var beltList = getBeltDegree(context);
    var clubs = getClubs(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // name
            Text(applocale!.translate('name'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _name,
              hint: applocale!.translate('your-name'),
              validator: (value) => namelValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // degree
            Text(applocale!.translate('belt-degree'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              hint: applocale!.translate('belt-degree'),
              validator: (value) => fieldValidator(value, context)!,
              items: beltList
                  .map((i) => DropdownMenuItem(
                        child: Text(i.name!),
                        value: i.value,
                      ))
                  .toList(),
              value: _beltDegree,
              onChange: (value) {
                setState(() {
                  _beltDegree = value;
                  print(_beltDegree);
                });
              },
            ),

            KMediumSizedBoxHeight,

            // organization
            Text(applocale!.translate('organization'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              hint: applocale!.translate('organization'),
              validator: (value) => fieldValidator(value, context)!,
              items: clubs
                  .map((i) => DropdownMenuItem(
                        child: Text(i.name!),
                        value: i.value,
                      ))
                  .toList(),
              value: _club,
              onChange: (value) {
                setState(() {
                  _club = value;
                  print(_club);
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> submit({String? name, String? belt, String? agency}) async {
    var appLocale = AppLocalizations.of(context);

    var isValide = _formKey.currentState!.validate();
    if (!isValide) {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    var register = await TrainingRegisterController()
        .registerInTraining(agency: agency, belt: belt, name: name);

    if (!register) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('could-not-register'),
        msg: appLocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('success'),
        titleColor: Colors.green,
        msg: appLocale!.translate('successfully-registered'),
        child: Icon(Icons.check_circle, color: Colors.green, size: 35),
      );
      clearData();
    }

    setState(() {
      _isLoading = false;
    });
  }

  void clearData() {
    setState(() {
      _name.clear();
      _club = null;
      _beltDegree = null;
    });
  }
}
