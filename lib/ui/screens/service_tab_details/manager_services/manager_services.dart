import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../widgets/circle_with_action.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/title_with_back_button.dart';

class ManagerServices extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(
                          appLocale!.translate('online-services-for-managers')),
                      KMediumSizedBoxHeight,

                      // body
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        alignment: WrapAlignment.center,
                        children: [
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('manager-registration-in-season'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('certificate-whom-it-may-concern'),
                            color: Colors.white,
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('manager-Card'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('Organization-Management-Courses'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale!.translate('remote-courses'),
                            onTap: () {},
                          ),
                          CircleWithAction(
                            size: 95,
                            title: appLocale
                                !.translate('Data-Sport-Authority-Subscribers'),
                            onTap: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
