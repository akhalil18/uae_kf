import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../../controller/champRegisterController.dart';
import '../../../../core/utils/dropDownLists.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../core/utils/validator.dart';
import '../../../../models/champ_registration.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/styles/text_style.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../../widgets/custom_drop_down_button.dart';
import '../../../widgets/title_with_back_button.dart';

class KumiteChamp extends StatefulWidget {
  final ChampRegistrationModel champRegistrationModel;
  KumiteChamp(this.champRegistrationModel);

  @override
  _KumiteChampState createState() => _KumiteChampState();
}

class _KumiteChampState extends State<KumiteChamp> with Validator {
  var _formKey = GlobalKey<FormState>();
  String _gender = 'male';
  String? _weight;

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // title
                      TitleWithBackButton(
                          AppLocalizations.of(context)!.translate('kumite')),

                      // body
                      KMediumSizedBoxHeight,
                      buildBody(context),

                      // Submit
                      KLargeSizedBoxHeight,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: ButtonWithGradient(
                          busy: _isLoading,
                          onTap: () {
                            submit(widget.champRegistrationModel, _gender,
                                _weight!);
                          },
                          title: AppLocalizations.of(context)
                              !.translate('subscribe'),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildBody(BuildContext context) {
    var weightList = getWeight(widget.champRegistrationModel.category!, _gender);
    var appLocale = AppLocalizations.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // gender
            Text(appLocale!.translate('gender'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: appLocale!.translate('gender'),
              items: [
                DropdownMenuItem(
                  child: Text(appLocale!.translate('male')),
                  value: 'male',
                ),
                DropdownMenuItem(
                  child: Text(appLocale!.translate('female')),
                  value: 'female',
                ),
              ],
              value: _gender,
              onChange: (value) {
                setState(() {
                  _weight = null;
                  _gender = value;
                });
                print(_gender);
              },
            ),

            KMediumSizedBoxHeight,

            // _weight
            Text(appLocale!.translate('weight'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: appLocale!.translate('weight'),
              items: weightList
                  .map(
                    (w) => DropdownMenuItem(
                      value: w,
                      child: Text('$w' + ' ${appLocale!.translate('kg')}'),
                    ),
                  )
                  .toList(),
              value: _weight,
              onChange: (value) {
                setState(() {
                  _weight = value;
                });
                print(_weight);
              },
            ),
          ],
        ),
      ),
    );
  }

  Future<void> submit(ChampRegistrationModel model, String selecteGender,
      String selectedWeight) async {
    var appLocale = AppLocalizations.of(context);

    var isValide = _formKey.currentState!.validate();
    if (!isValide) {
      return;
    }

    // add selected gender & weight to model
    model
      ..gender = selecteGender
      ..weight = selectedWeight;

    setState(() {
      _isLoading = true;
    });

    var register =
        await ChampRegisterController().registerInChampionship(model);
    if (register == null) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('could-not-register'),
        msg: appLocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    } else if (!register) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('failed'),
        titleColor: Colors.red,
        msg: appLocale!.translate('already-register'),
      );
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('success'),
        titleColor: Colors.green,
        msg: appLocale!.translate('successfully-registered'),
        child: Icon(Icons.check_circle, color: Colors.green, size: 35),
      );
      clearData();
    }

    setState(() {
      _isLoading = false;
    });
  }

  void clearData() {
    setState(() {
      _weight = null;
    });
  }
}
