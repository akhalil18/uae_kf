import 'package:flutter/material.dart';

import '../../../../controller/champRegisterController.dart';
import '../../../../core/localization/app_localizations.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../core/utils/validator.dart';
import '../../../../models/champ_registration.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/styles/text_style.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../../widgets/custom_drop_down_button.dart';
import '../../../widgets/title_with_back_button.dart';

class KataChamp extends StatefulWidget {
  final ChampRegistrationModel champRegistrationModel;

  KataChamp(this.champRegistrationModel);

  @override
  _KataChampState createState() => _KataChampState();
}

class _KataChampState extends State<KataChamp> with Validator {
  String? _gender;
  var _isLoading = false;

  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(
                          AppLocalizations.of(context)!.translate('kata')),
                      KMediumSizedBoxHeight,
                      buildBody(context),

                      // Submit
                      KLargeSizedBoxHeight,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: ButtonWithGradient(
                          busy: _isLoading,
                          onTap: () {
                            submit(widget.champRegistrationModel, _gender!);
                          },
                          title: AppLocalizations.of(context)
                              !.translate('subscribe'),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // gender
            Text(appLocale!.translate('gender'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              hint: appLocale!.translate('gender'),
              validator: (value) => fieldValidator(value, context)!,
              items: [
                DropdownMenuItem(
                  child: Text(appLocale!.translate('male')),
                  value: 'male',
                ),
                DropdownMenuItem(
                  child: Text(appLocale!.translate('female')),
                  value: 'female',
                ),
              ],
              value: _gender,
              onChange: (value) {
                setState(() {
                  _gender = value;
                });
              },
            ),

            KMediumSizedBoxHeight,
          ],
        ),
      ),
    );
  }

  Future<void> submit(ChampRegistrationModel model, String gender) async {
    var appLocale = AppLocalizations.of(context);

    var isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    // add selected gender to model
    model.gender = gender;

    setState(() {
      _isLoading = true;
    });

    var register =
        await ChampRegisterController().registerInChampionship(model);
    if (register == null) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('could-not-register'),
        msg: appLocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    } else if (!register) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('failed'),
        titleColor: Colors.red,
        msg: appLocale!.translate('already-register'),
      );
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('success'),
        titleColor: Colors.green,
        msg: appLocale!.translate('successfully-registered'),
        child: Icon(Icons.check_circle, color: Colors.green, size: 35),
      );
      clearData();
    }

    setState(() {
      _isLoading = false;
    });
  }

  void clearData() {
    setState(() {
      _gender = null;
    });
  }
}
