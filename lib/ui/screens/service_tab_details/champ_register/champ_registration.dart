import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/models/champ_registration.dart';
import 'package:flutter/material.dart';

import '../../../../controller/get_championships_controller.dart';
import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../models/modelChampionships.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/circle_with_action.dart';
import '../../../widgets/error_widget.dart';
import '../../../widgets/title_with_back_button.dart';
import 'select_category.dart';

class ChampRegistration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
                alignment: Alignment.center,
                color: AppColors.primaryColor,
                height: 85,
                child: AppHeader()),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: FutureBuilder(
                  future: GetChampionshipsController().getallChampionships(),
                  builder: (_, AsyncSnapshot<List<Championship>> snapshot) {
                    var champList = snapshot.data;

                    return snapshot.connectionState != ConnectionState.done
                        ? Center(child: CircularProgressIndicator())
                        : !snapshot.hasData
                            ? AppErrorWidget(textColor: Colors.white)
                            : SingleChildScrollView(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Column(
                                  children: [
                                    TitleWithBackButton(applocale!.translate(
                                        'registration-at-championship')),
                                    KMediumSizedBoxHeight,

                                    // body
                                    Wrap(
                                      crossAxisAlignment:
                                          WrapCrossAlignment.start,
                                      alignment: WrapAlignment.center,
                                      children: champList
                                          !.map(
                                            (c) => CircleWithAction(
                                              title: c.name!,
                                              size: 95,
                                              onTap: () {
                                                GoTo.screen(
                                                  context,
                                                  SelectCategory(
                                                      ChampRegistrationModel(
                                                          champId: c.id,
                                                          champName: c.name)),
                                                );
                                              },
                                            ),
                                          )
                                          .toList(),
                                    ),
                                  ],
                                ),
                              );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
