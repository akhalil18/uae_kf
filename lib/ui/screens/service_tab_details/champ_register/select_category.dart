import 'package:UAE_KF/core/localization/app_localizations.dart';

import '../../../../core/utils/GoTo.dart';
import '../../../../models/champ_registration.dart';
import 'champ_type.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../widgets/circle_with_action.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/title_with_back_button.dart';

class SelectCategory extends StatelessWidget {
  final ChampRegistrationModel champRegistrationModel;
  SelectCategory(this.champRegistrationModel);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(champRegistrationModel.champName!),
                      KMediumSizedBoxHeight,

                      // body
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        alignment: WrapAlignment.center,
                        children: [
                          CircleWithAction(
                            title: applocale!.translate('Under-14-circle'),
                            onTap: () {
                              this.champRegistrationModel.category =
                                  'cubs_and_girls';
                              GoTo.screen(
                                  context,
                                  ChampType(
                                      title: applocale!.translate('Under-14'),
                                      champRegistrationModel:
                                          champRegistrationModel));
                            },
                          ),
                          CircleWithAction(
                            title: applocale!.translate('Cadets-circle'),
                            color: Colors.white,
                            onTap: () {
                              this.champRegistrationModel.category = 'budding';
                              GoTo.screen(
                                  context,
                                  ChampType(
                                      title: applocale!.translate('Cadets'),
                                      champRegistrationModel:
                                          champRegistrationModel));
                            },
                          ),
                          CircleWithAction(
                            title: applocale!.translate('Juniors-circle'),
                            onTap: () {
                              this.champRegistrationModel.category = 'youth';
                              GoTo.screen(
                                  context,
                                  ChampType(
                                      title: applocale!.translate('Juniors'),
                                      champRegistrationModel:
                                          champRegistrationModel));
                            },
                          ),
                          CircleWithAction(
                            title: applocale!.translate('Seniors-circle'),
                            onTap: () {
                              this.champRegistrationModel.category =
                                  'men_and_women';
                              GoTo.screen(
                                  context,
                                  ChampType(
                                      title: applocale!.translate('Seniors'),
                                      champRegistrationModel:
                                          champRegistrationModel));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
