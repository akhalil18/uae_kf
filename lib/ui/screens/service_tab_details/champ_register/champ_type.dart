import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/models/champ_registration.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../widgets/circle_with_action.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/title_with_back_button.dart';
import 'kata_Champ.dart';
import 'kumite.champ.dart';

class ChampType extends StatelessWidget {
  final ChampRegistrationModel? champRegistrationModel;
  final String? title;

  ChampType({this.champRegistrationModel, this.title});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      TitleWithBackButton(title!),
                      KMediumSizedBoxHeight,

                      // body
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        alignment: WrapAlignment.center,
                        children: [
                          CircleWithAction(
                            title:
                                AppLocalizations.of(context)!.translate('kata'),
                            onTap: () {
                              champRegistrationModel!.champType = 'kata';
                              GoTo.screen(
                                  context, KataChamp(champRegistrationModel!));
                            },
                          ),
                          CircleWithAction(
                            title: AppLocalizations.of(context)
                                !.translate('kumite'),
                            color: Colors.white,
                            onTap: () {
                              champRegistrationModel!.champType = 'kumite';
                              GoTo.screen(
                                  context, KumiteChamp(champRegistrationModel!));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
