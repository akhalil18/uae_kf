import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../controller/get_courses_controller.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/error_widget.dart';
import '../../../widgets/title_with_back_button.dart';

class RemoteCourses extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: FutureBuilder(
                  future: GetCoursesController().getAllCourses(),
                  builder: (_,snapshot) => (snapshot.connectionState ==
                          ConnectionState.waiting)
                      ? Center(child: CircularProgressIndicator())
                      : (!snapshot.hasData)
                          ? AppErrorWidget(textColor: Colors.white)
                          : SingleChildScrollView(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TitleWithBackButton(
                                      AppLocalizations.of(context)
                                          !.translate('remote-courses')),

                                  KMediumSizedBoxHeight,

                                  // Courses List
                                  ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: (snapshot.data as List).length,
                                    itemBuilder: (context, i) => courseCard(
                                      (snapshot.data as List)[i].name,
                                      (snapshot.data as List)[i].link,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container courseCard(String name, String link) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            name,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 16.0,
              color: AppColors.primaryColor,
            ),
          ),
          GestureDetector(
            onTap: () async {
              if (await canLaunch(link)) {
                await launch(link);
              } else {
                throw 'Could not launch $link';
              }
            },
            child: Text(
              link,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14.0,
                height: 1.5,
                decoration: TextDecoration.underline,
                color: Color(0xff2f4299),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
