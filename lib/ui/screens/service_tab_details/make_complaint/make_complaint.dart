import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../../controller/makeComplaintController.dart';
import '../../../../core/utils/dropDownLists.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../core/utils/validator.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/styles/text_style.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../../widgets/custom_drop_down_button.dart';
import '../../../widgets/custom_text_form_field.dart';
import '../../../widgets/title_with_back_button.dart';

class MakeComplaint extends StatefulWidget {
  @override
  _MakeComplaintState createState() => _MakeComplaintState();
}

class _MakeComplaintState extends State<MakeComplaint> with Validator {
  var _name = TextEditingController();
  var _complaint = TextEditingController();
  String? _club;
  String? _beltDegree;
  String? _category;

  var _formKey = GlobalKey<FormState>();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(AppLocalizations.of(context)
                          !.translate('make-complaint')),

                      // body
                      buildBody(context),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildBody(BuildContext context) {
    var applocale = AppLocalizations.of(context);

    var beltList = getBeltDegree(context);
    var clubs = getClubs(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // name
            Text(applocale!.translate('name'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _name,
              hint: applocale!.translate('your-name'),
              validator: (value) => namelValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // degree
            Text(applocale!.translate('belt-degree'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: applocale!.translate('belt-degree'),
              items: beltList
                  .map((i) => DropdownMenuItem(
                        child: Text(i.name!),
                        value: i.value,
                      ))
                  .toList(),
              value: _beltDegree,
              onChange: (value) {
                setState(() {
                  _beltDegree = value;
                });
              },
            ),

            KMediumSizedBoxHeight,

            // type
            Text(applocale!.translate('category'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: applocale!.translate('category'),
              items: [
                DropdownMenuItem(
                  child: Text(applocale!.translate('player')),
                  value: 'player',
                ),
                DropdownMenuItem(
                  child: Text(applocale!.translate('coach')),
                  value: 'coach',
                ),
                DropdownMenuItem(
                  child: Text(applocale!.translate('referee')),
                  value: 'referee',
                ),
              ],
              value: _category,
              onChange: (value) {
                setState(() {
                  _category = value;
                });
              },
            ),

            KMediumSizedBoxHeight,

            // organization
            Text(applocale!.translate('organization'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: applocale!.translate('organization'),
              items: clubs
                  .map((i) => DropdownMenuItem(
                        child: Text(i.name!),
                        value: i.value,
                      ))
                  .toList(),
              value: _club,
              onChange: (value) {
                setState(() {
                  _club = value;
                });
              },
            ),

            KMediumSizedBoxHeight,

            // complain
            Text(applocale!.translate('complaint-message'),
                style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _complaint,
              lines: 3,
              keyboardType: TextInputType.multiline,
              validator: (value) => messageValidator(value!, context),
            ),

            KLargeSizedBoxHeight,

            //send
            ButtonWithGradient(
              busy: _isLoading,
              onTap: () {
                _send(
                    context: context,
                    complaint: _complaint.text,
                    name: _name.text,
                    agency: _club!,
                    belt: _beltDegree!,
                    category: _category!);
              },
              title: applocale!.translate('send'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _send({
    BuildContext? context,
    String? name,
    String? belt,
    String? agency,
    String? complaint,
    String? category,
  }) async {
    var appLocale = AppLocalizations.of(context!);

    var isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }

    setState(() {
      _isLoading = true;
    });
    final statue = await MakeComplaintController().makeComplaint(
        name: name,
        belt: belt,
        complaint: complaint,
        agency: agency,
        category: category);
    if (statue) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('success'),
        titleColor: Colors.green,
        msg: appLocale!.translate('message-sent-successfully'),
        child: Icon(Icons.check_circle_outline, color: Colors.green, size: 35),
      );
      celarData();
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('not-sent'),
        msg: appLocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }

  void celarData() {
    setState(() {
      _name.clear();
      _complaint.clear();
      _club = null;
      _beltDegree = null;
      _category = null;
    });
  }
}
