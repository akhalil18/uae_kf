import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../models/ModelActivities.dart';
import 'show_image.dart';

class GalleryTab extends StatefulWidget {
  final List<MMedia>? listOfImages;

  const GalleryTab({this.listOfImages});
  @override
  _GalleryTabState createState() => _GalleryTabState();
}

class _GalleryTabState extends State<GalleryTab> {
  String? selectedImage;

  @override
  void initState() {
    super.initState();
    if (widget.listOfImages!.isNotEmpty)
      selectedImage = widget.listOfImages![0].file;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: widget.listOfImages!.isEmpty
          ? Text(AppLocalizations.of(context)!.translate('no-images'),
              style: TextStyle(color: Colors.white))
          : Column(
              children: [
                GestureDetector(
                  onTap: () {
                    GoTo.screen(context, ShowImage(image: selectedImage!));
                  },
                  child: Hero(
                    tag: 'imagesLibrary',
                    child: Container(
                      width: double.infinity,
                      child: AspectRatio(
                        aspectRatio: ScreenUtil.portrait! ? 2 : 3,
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          child: Image.network(
                            selectedImage!, fit: BoxFit.cover,
                            // width: double.infinity,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                GridView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: ScreenUtil.portrait! ? 3 : 4,
                      childAspectRatio: 0.95,
                      crossAxisSpacing: 10.0,
                      mainAxisSpacing: 15,
                    ),
                    itemCount: widget.listOfImages!.length,
                    itemBuilder: (ctx, index) {
                      return gridItem(image: widget.listOfImages![index].file!);
                    }),
              ],
            ),
    );
  }

  Widget gridItem({String? image}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = image;
        });
      },
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        child: Image.network(
          image!,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
