import 'package:flutter/material.dart';

import '../../../shared/styles/app_colors.dart';

class ShowImage extends StatelessWidget {
  final String? image;

  const ShowImage({this.image});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(gradient: AppColors.backgroundGradient),
          child: Stack(
            children: [
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Hero(
                  tag: 'imagesLibrary',
                  child: Image.network(
                    image!, fit: BoxFit.contain,
                    // width: double.infinity,
                  ),
                ),
              ),
              Align(
                alignment: AlignmentDirectional.topStart,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: IconButton(
                    color: Colors.white,
                    icon: Icon(
                      Icons.arrow_back,
                      size: 28,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
