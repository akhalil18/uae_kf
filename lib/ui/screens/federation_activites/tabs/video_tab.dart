import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/ui/shared/styles/app_colors.dart';
import 'package:flutter/material.dart';

import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../models/ModelActivities.dart';
import 'show_video.dart';

class VideoTab extends StatefulWidget {
  final List<MMedia>? listOfVideos;

  const VideoTab({this.listOfVideos});
  @override
  _VideoTabState createState() => _VideoTabState();
}

class _VideoTabState extends State<VideoTab> {
  MMedia? selectedVideo;

  @override
  void initState() {
    super.initState();
    if (widget.listOfVideos!.isNotEmpty) selectedVideo = widget.listOfVideos![0];
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: widget.listOfVideos!.isEmpty
          ? Text(AppLocalizations.of(context)!.translate('no-videos'),
              style: TextStyle(color: Colors.white))
          : Column(
              children: [
                GestureDetector(
                  onTap: () {
                    GoTo.screen(
                      context,
                      ShowVideo(videoURL: selectedVideo!.file),
                    );
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: double.infinity,
                        child: AspectRatio(
                          aspectRatio: ScreenUtil.portrait! ? 2 : 3,
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            child: Image.network(
                              selectedVideo!.image!, fit: BoxFit.cover,
                              // width: double.infinity,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.play_circle_filled,
                          color: AppColors.secondaryColor,
                          size: 40,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 15),
                GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: ScreenUtil.portrait! ? 3 : 4,
                    childAspectRatio: 0.95,
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 15,
                  ),
                  itemCount: widget.listOfVideos!.length,
                  itemBuilder: (context, index) =>
                      gridItem(video: widget.listOfVideos![index]),
                ),
              ],
            ),
    );
  }

  Widget gridItem({MMedia? video}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedVideo = video;
        });
      },
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        child: Image.network(
          video!.image!,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
