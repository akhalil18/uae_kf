import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../../../shared/styles/app_colors.dart';

class ShowVideo extends StatefulWidget {
  final String? videoURL;

  const ShowVideo({this.videoURL});

  @override
  _ShowVideoState createState() => _ShowVideoState();
}

class _ShowVideoState extends State<ShowVideo> {
  VideoPlayerController? _controller;
  bool isInit = false;
  bool iconHide = true;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.videoURL!)
      ..initialize().then((_) {
        setState(() {
          isInit = true;
          _controller!.play();
        });
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Container(
          child: isInit
              ? InkWell(
                  onTap: () {
                    setState(() {
                      _controller!.value.isPlaying
                          ? _controller!.pause()
                          : _controller!.play();
                      iconHide = false;
                    });
                    Future.delayed(Duration(seconds: 1), () {
                      setState(() {
                        iconHide = true;
                      });
                    });
                  },
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: AspectRatio(
                        aspectRatio: _controller!.value.aspectRatio,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            VideoPlayer(_controller!),
                            !iconHide
                                ? Icon(
                                    _controller!.value.isPlaying
                                        ? Icons.pause
                                        : Icons.play_arrow,
                                    color: AppColors.primaryColor,
                                    size: 35)
                                : Container(),
                            Align(
                                alignment: Alignment.bottomCenter,
                                child: VideoProgressIndicator(
                                  _controller!,
                                  allowScrubbing: true,
                                )),
                          ],
                        )),
                  ),
                )
              : Center(
                  child: Container(
                  child: Text(
                    "Loading..",
                    style: TextStyle(color: Colors.white),
                  ),
                )),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller!.dispose();
  }
}
