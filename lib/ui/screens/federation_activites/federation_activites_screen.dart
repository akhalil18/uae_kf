import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/ui/widgets/error_widget.dart';
import 'package:flutter/material.dart';

import '../../../controller/home_page_controller.dart';
import '../../../core/utils/enums.dart';
import '../../../core/utils/screen_util.dart';
import '../../../models/ModelActivities.dart';
import '../../shared/styles/app_colors.dart';
import '../../widgets/app_header.dart';
import '../../widgets/title_with_back_button.dart';
import 'tabs/gallery_tab.dart';
import 'tabs/video_tab.dart';

class FederationActivitesScreen extends StatefulWidget {
  final int screenNum; // 1 = FederationActivites && 2 = InternationalAgenda

  const FederationActivitesScreen({ this.screenNum = 1})
       ;

  @override
  _FederationActivitesScreenState createState() =>
      _FederationActivitesScreenState();
}

class _FederationActivitesScreenState extends State<FederationActivitesScreen> {
  var selectedTab = HeaderTabs.galleryTab;
  Future<ActivitiesData>? getActivites;

  @override
  void initState() {
    super.initState();
    getActivites = widget.screenNum == 1
        ? HomePageController().getFederationActivities()
        : HomePageController().getInternationalAgenda();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: FutureBuilder(
                  future: getActivites,
                  builder: (ctx, snapShpt) => snapShpt.connectionState !=
                          ConnectionState.done
                      ? Center(child: CircularProgressIndicator())
                      : !snapShpt.hasData
                          ? AppErrorWidget(textColor: Colors.white)
                          : SingleChildScrollView(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                              child: Column(
                                children: [
                                  widget.screenNum == 1
                                      ? TitleWithBackButton(appLocale
                                          !.translate('federation-activities'))
                                      : TitleWithBackButton(appLocale
                                          !.translate('international-agenda')),
                                  SizedBox(height: 15),

                                  // switch tab buttons
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: buildTabButton(
                                              text: appLocale
                                                  !.translate('images-library'),
                                              backgroundColor: selectedTab ==
                                                      HeaderTabs.galleryTab
                                                  ? AppColors.accentColor
                                                  : Colors.white,
                                              onTab: () {
                                                setState(() {
                                                  selectedTab =
                                                      HeaderTabs.galleryTab;
                                                });
                                              },
                                            ),
                                          ),
                                          Expanded(
                                            child: buildTabButton(
                                              text: appLocale
                                                  !.translate('videos-library'),
                                              backgroundColor: selectedTab ==
                                                      HeaderTabs.videoTab
                                                  ? AppColors.accentColor
                                                  : Colors.white,
                                              onTab: () {
                                                setState(() {
                                                  selectedTab =
                                                      HeaderTabs.videoTab;
                                                });
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 15),

                                  // Tab Body
                                  buildPageBody(snapShpt.data as ActivitiesData),
                                ],
                              ),
                            ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPageBody(ActivitiesData data) {
    if (selectedTab == HeaderTabs.galleryTab) {
      return GalleryTab(listOfImages: data.images);
    } else {
      return VideoTab(listOfVideos: data.videos);
    }
  }

  Container buildTabButton(
      {Color? backgroundColor, Function? onTab, String? text}) {
    return Container(
      height: 40,
      child: RaisedButton(
        onPressed: ()=>onTab!,
        color: backgroundColor,
        child: Text(
          text!,
          style: TextStyle(
            fontSize: 13.0,
            fontWeight: FontWeight.w600,
            color:
                backgroundColor == Colors.white ? Colors.black : Colors.white,
          ),
        ),
      ),
    );
  }
}
