import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../auth/authentication_service.dart';
import '../../../controller/reset_password_controller.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/app_header.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/password_text_form_field.dart';
import '../../widgets/title_with_back_button.dart';

class MoreChangePassScreen extends StatefulWidget {
  @override
  _MoreChangePassScreenState createState() => _MoreChangePassScreenState();
}

class _MoreChangePassScreenState extends State<MoreChangePassScreen>
    with Validator {
  final _formKey = GlobalKey<FormState>();
  var _password = TextEditingController();
  var _confirmPassword = TextEditingController();
  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(AppLocalizations.of(context)
                          !.translate('change-password')),

                      // body
                      buildBody(context),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    var currentUser =
        Provider.of<AuthenticationService>(context, listen: false).user;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // password
            Text(appLocale!.translate('new-password'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _password,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('new-password'),
              validator: (value) => passwordValidator(value!, context)!,
            ),

            KMediumSizedBoxHeight,

            // Confirm pass
            Text(appLocale!.translate('confirm-password'),
                style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _confirmPassword,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('confirm-password'),
            ),

            KLargeSizedBoxHeight,

            // save
            ButtonWithGradient(
              busy: isLoading,
              title: AppLocalizations.of(context)!.translate('save'),
              onTap: () {
                changePassword(
                    context: context,
                    password: _password.text,
                    confirmPassword: _confirmPassword.text,
                    email: currentUser!.email!);
              },
            ),
          ],
        ),
      ),
    );
  }

  changePassword({
    BuildContext? context,
    String? email,
    String? password,
    String? confirmPassword,
  }) async {
    var appLocale = AppLocalizations.of(context!);

    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    } else if (password != confirmPassword) {
      UI.dialog(
          context: context, msg: appLocale!.translate("passwords-don't-match"));
    } else {
      setState(() {
        isLoading = true;
      });
      final statue =
          await ResetPasswordController().resetPassword(email!, password!);
      if (statue) {
        UI.dialog(
          context: context,
          title: appLocale!.translate('success'),
          titleColor: Colors.green,
          msg: appLocale!.translate('password-changed-successfully'),
          child:
              Icon(Icons.check_circle_outline, color: Colors.green, size: 35),
        );
        clearData();
      } else {
        UI.dialog(
          context: context,
          title: appLocale!.translate('failed'),
          msg: appLocale!.translate('try-again-message'),
          child: Icon(Icons.error_outline, color: Colors.red, size: 35),
        );
      }
      setState(() {
        isLoading = false;
      });
    }
  }

  void clearData() {
    setState(() {
      _password.clear();
      _confirmPassword.clear();
    });
  }
}
