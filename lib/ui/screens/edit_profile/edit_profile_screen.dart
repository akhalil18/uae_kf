import 'dart:io';

import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../controller/update_profile_controller.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../../models/user.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/app_header.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_drop_down_button.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/title_with_back_button.dart';
import '../main_pages/main_screen.dart';

class EditProfileScreen extends StatefulWidget {
  final User? currentUser;

  EditProfileScreen({this.currentUser});

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> with Validator {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? _name;
  TextEditingController? _identify;
  TextEditingController? _email;
  TextEditingController? _phone;

  File? _selectedImage;
  String? _gender;
  String? _profileImage;

  // Country _selected;

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    // _selected = Country.findByIsoCode("EG");

    _gender = widget.currentUser!.gender;
    _profileImage = widget.currentUser!.image;
    _name = TextEditingController(text: widget.currentUser!.name);
    _identify = TextEditingController(text: widget.currentUser!.identity);
    _email = TextEditingController(text: widget.currentUser!.email);
    _phone = TextEditingController(text: widget.currentUser!.phone);
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TitleWithBackButton(
                            appLocale!.translate('edit-profile')),
                        KMediumSizedBoxHeight,

                        // Image
                        Row(
                          children: [
                            Stack(
                              overflow: Overflow.visible,
                              children: [
                                GestureDetector(
                                  onTap: () => _showImageDialog(context),
                                  child: Container(
                                    width: 55,
                                    height: 55,
                                    decoration: BoxDecoration(
                                      color: Colors.blueGrey[200],
                                      shape: BoxShape.circle,
                                      border: Border.all(color: Colors.grey),
                                      image: _selectedImage == null
                                          ? _profileImage == null
                                              ? null
                                              : DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: NetworkImage(
                                                      _profileImage!))
                                          : DecorationImage(
                                              fit: BoxFit.fill,
                                              image: FileImage(_selectedImage!)),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  top: -8,
                                  child: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _selectedImage = null;
                                        _profileImage = null;
                                      });
                                    },
                                    child: CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 12,
                                      child: Icon(
                                        Icons.close,
                                        color: Colors.grey,
                                        size: 15,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(width: 10),
                            Text(appLocale!.translate('edit-profile-image'),
                                style: KProfileFieldHint),
                          ],
                        ),

                        KMediumSizedBoxHeight,

                        // User name
                        Text(appLocale!.translate('name'),
                            style: KProfileFieldHint),
                        KSmallSizedBoxHeight,
                        CustomTextFormField(
                          controller: _name,
                          icon: Icon(Icons.perm_identity),
                          hint: appLocale!.translate('name'),
                          validator: (value) => namelValidator(value!, context),
                        ),

                        KMediumSizedBoxHeight,

                        // identify number
                        Text(appLocale!.translate('identification-number'),
                            style: KProfileFieldHint),
                        KSmallSizedBoxHeight,
                        CustomTextFormField(
                          controller: _identify,
                          icon: Icon(Icons.folder_shared),
                          hint: appLocale!.translate('identification-number'),
                        ),

                        KMediumSizedBoxHeight,

                        // Phone number
                        Text(appLocale!.translate('phone-number'),
                            style: KProfileFieldHint),
                        KSmallSizedBoxHeight,
                        phoneField(context),

                        KMediumSizedBoxHeight,

                        // Email
                        Text(appLocale!.translate('email'),
                            style: KProfileFieldHint),
                        KSmallSizedBoxHeight,
                        CustomTextFormField(
                          controller: _email,
                          icon: Icon(Icons.email),
                          hint: appLocale!.translate('email'),
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) => emailValidator(value!, context),
                        ),

                        KMediumSizedBoxHeight,

                        // Type
                        Text(appLocale!.translate('gender'),
                            style: KProfileFieldHint),
                        KSmallSizedBoxHeight,
                        CustomDropDownButton(
                          hint: _gender!.isNotEmpty
                              ? appLocale!.translate(_gender!)
                              : appLocale!.translate('gender'),
                          items: [
                            DropdownMenuItem(
                              child: Text(appLocale!.translate('male')),
                              value: 'male',
                            ),
                            DropdownMenuItem(
                              child: Text(appLocale!.translate('female')),
                              value: 'female',
                            ),
                          ],
                          onChange: (value) {
                            setState(() {
                              _gender = value;
                              print(_gender);
                            });
                          },
                        ),

                        KLargeSizedBoxHeight,
                        ButtonWithGradient(
                          title: appLocale!.translate('save'),
                          busy: isLoading,
                          onTap: () {
                            print(_phone!.text);
                            updateProfile(
                                context: context,
                                email: _email!.text,
                                gender: _gender!,
                                identity: _identify!.text,
                                image: _selectedImage!,
                                name: _name!.text,
                                phone: _phone!.text);
                          },
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container phoneField(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              height: 45,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.black54),
              ),
              child: Text('‎+971'),
              //  CountryPicker(
              //   dialingCodeTextStyle:
              //       SizeandStyleUtills().getTextStyleRegular(fontsize: 18),
              //   dense: false,
              //   showFlag: false,
              //   //displays flag, true by default
              //   showDialingCode: true,
              //   //displays dialing code, false by default
              //   showName: false,
              //   //displays country name, true by default
              //   showCurrency: false,
              //   //eg. 'British pound'
              //   showCurrencyISO: false,
              //   //eg. 'GBP'
              //   onChanged: (Country country) {
              //     setState(() {
              //       _selected = country;
              //     });
              //   },
              //   selectedCountry: _selected,
              // ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            flex: 6,
            child: CustomTextFormField(
              controller: _phone,
              hint: AppLocalizations.of(context)!.translate('phone-number'),
              keyboardType: TextInputType.phone,
              // validator: (value) => mobileValidator(value, context),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _showImageDialog(BuildContext context) async {
    var appLocale = AppLocalizations.of(context);

    return showDialog(
        context: context,
        builder: (ctx) {
          return SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text(
              appLocale!.translate('choose-from'),
              style: TextStyle(color: AppColors.accentColor),
            ),
            children: <Widget>[
              SimpleDialogOption(
                child: Text(appLocale!.translate('camera')),
                onPressed: () async {
                  Navigator.of(ctx).pop();
                  pickImage(ImageSource.camera);
                },
              ),
              SimpleDialogOption(
                child: Text(appLocale!.translate('gallery')),
                onPressed: () async {
                  Navigator.of(ctx).pop();
                  pickImage(ImageSource.gallery);
                },
              ),
            ],
          );
        });
  }

  Future<void> pickImage(ImageSource source) async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(
      source: source,
      maxWidth: 960,
      maxHeight: 675,
    );
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _selectedImage = File(pickedFile.path);
    });
  }

  Future<void> updateProfile({
    BuildContext? context,
    String? name,
    String? email,
    String? phone,
    String? gender,
    String? identity,
    File? image,
  }) async {
    final isValid = _formKey.currentState!.validate();
    var appLocale = AppLocalizations.of(context!);

    if (!isValid) {
      return;
    } else {
      // if all fields is valid
      setState(() {
        isLoading = true;
      });
      bool statue = await UpdateProfileController().updatProfile(
          context: context,
          name: name,
          email: email,
          phone: phone,
          gender: gender,
          identity: identity,
          image: image);

      if (statue) {
        GoTo.screenAndRemoveUntil(context, MainScreen(tabIndex: 2));
      } else {
        UI.dialog(
          context: context,
          title: appLocale!.translate('failed'),
          msg: appLocale!.translate('try-again-message'),
          child: Icon(Icons.error_outline, color: Colors.red, size: 35),
        );
      }
      setState(() {
        isLoading = false;
      });
    }
  }
}
