import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/core/utils/AssetsRoutes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../shared/styles/app_colors.dart';
import 'Tabs/HomeScreen.dart';
import 'Tabs/MoreScreen.dart';
import 'Tabs/ProfileScreen.dart';
import 'Tabs/ServicesScreen.dart';

class MainScreen extends StatefulWidget {
  final int tabIndex;

  const MainScreen({ this.tabIndex = 0});
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int pageIndex = 0;

  List<Widget> _pages = [
    HomeScreen(),
    ServicesScreen(),
    ProfileScreen(),
    MoreScreen(),
  ];

  List<BottomNavigationBarItem> navigationBarItems(BuildContext context) {
    var locale = AppLocalizations.of(context);
    return [
      BottomNavigationBarItem(
        label: locale!.translate('home'),
        activeIcon: Image.asset(AssetsRoutes.home),
        icon: Image.asset(
          AssetsRoutes.home,
          color: AppColors.secondaryColor,
        ),
      ),
      BottomNavigationBarItem(
        label: locale!.translate('services'),
        activeIcon: Image.asset(
          AssetsRoutes.services,
          color: AppColors.accentColor,
        ),
        icon: Image.asset(
          AssetsRoutes.services,
        ),
      ),
      BottomNavigationBarItem(
        label: locale!.translate('profile'),
        activeIcon: Image.asset(
          AssetsRoutes.account,
          color: AppColors.accentColor,
        ),
        icon: Image.asset(AssetsRoutes.account),
      ),
      BottomNavigationBarItem(
        label: locale!.translate('more'),
        activeIcon: Image.asset(
          AssetsRoutes.more,
          color: AppColors.accentColor,
        ),
        icon: Image.asset(
          AssetsRoutes.more,
        ),
      ),
    ];
  }

  @override
  void initState() {
    pageIndex = widget.tabIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: AppColors.primaryColor));

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 30,
        selectedItemColor: AppColors.accentColor,
        showUnselectedLabels: true,
        unselectedItemColor: AppColors.secondaryColor,
        selectedLabelStyle: TextStyle(
          fontSize: 12,
          fontWeight: FontWeight.w600,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 11.5,
          fontWeight: FontWeight.w600,
        ),
        currentIndex: pageIndex,
        onTap: onTabSelected,
        items: navigationBarItems(context),
      ),
      body: _pages[pageIndex],
    );
  }

  void onTabSelected(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
  }
}
