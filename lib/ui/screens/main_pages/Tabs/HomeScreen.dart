import 'dart:io';
import 'dart:typed_data';

import 'package:UAE_KF/ui/screens/events_volunteers/events_volunteers.dart';
import 'package:UAE_KF/ui/screens/federation_activites/tabs/show_video.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import '../../../../auth/authentication_service.dart';
import '../../../../controller/home_page_controller.dart';
import '../../../../core/localization/app_localizations.dart';
import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../models/home_page.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/error_widget.dart';
import '../../federation_activites/federation_activites_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<HomeData>? getHomePage;

  final List<String> imgList = [
    'assets/images/img2.png',
    'assets/images/img4.png',
    'assets/images/img5.png',
    'assets/images/img6.png'
  ];

  int _currentImage = 0;
  int? _downloadingFileIndex;

  @override
  void initState() {
    super.initState();
    getHomePage = HomePageController().getHomePageData();
  }

  @override
  Widget build(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    ScreenUtil.init(context);
    final currentUser =
        Provider.of<AuthenticationService>(context, listen: false).user;

    return Container(
      width: ScreenUtil.screenWidthDp,
      height: ScreenUtil.screenHeightDp,
      padding:
          EdgeInsets.only(top: ScreenUtil.statusBarHeight! + 10, bottom: 10),
      decoration: BoxDecoration(
          gradient: LinearGradient(
        colors: [
          Color(0xff0e1220),
          Color(0xff82161a2d),
          // Color(0xff59c5c2c9),
          Color(0xff5f6683),
          // Color(0xff1e284b),
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      )),
      child: FutureBuilder(
        future: getHomePage,
        builder: (BuildContext context, AsyncSnapshot<HomeData> snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? Center(child: CircularProgressIndicator())
              : !snapshot.hasData
                  ? AppErrorWidget(textColor: Colors.white)
                  : SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          // Header
                          AppHeader(),

                          // Name and Image
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 20, horizontal: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        applocale!.translate('welcome'),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20.0,
                                        ),
                                      ),
                                      Text(
                                        currentUser?.name ??
                                            applocale!.translate('visitor'),
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 19.0,
                                          fontWeight: FontWeight.w900,
                                          height: 1.5,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                CircleAvatar(
                                  radius: 27.5,
                                  backgroundColor: Color(0xff59c5c2c9),
                                  backgroundImage: currentUser == null
                                      ? null
                                      : NetworkImage(currentUser.image!),
                                ),
                              ],
                            ),
                          ),

                          // Cover Image
                          Container(
                            height: ScreenUtil.portrait!
                                ? ScreenUtil.screenHeightDp! * .23
                                : ScreenUtil.screenHeightDp! * .5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(13),
                                  topRight: Radius.circular(13)),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(snapshot.data!.poster!),
                              ),
                            ),
                          ),

                          KLargeSizedBoxHeight,

                          // Slider
                          buildImageSlider(snapshot.data!.banners!),

                          KMediumSizedBoxHeight,

                          // Footer Circles
                          Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            alignment: WrapAlignment.center,
                            children: [
                              buildGridCircle(
                                index: 1,
                                title: appLocale
                                    .translate('federation-activities-circle'),
                                color: Color(0xff229c73),
                                onTap: () {
                                  GoTo.screen(
                                      context,
                                      FederationActivitesScreen(
                                        screenNum: 1,
                                      ));
                                },
                              ),
                              buildGridCircle(
                                index: 2,
                                title: appLocale
                                    .translate('international-agenda-circle'),
                                color: Colors.white,
                                onTap: () {
                                  GoTo.screen(
                                      context,
                                      FederationActivitesScreen(
                                        screenNum: 2,
                                      ));
                                },
                              ),
                              buildGridCircle(
                                index: 3,
                                title: applocale!.translate('union-list'),
                                color: Color(0xffc93737),
                                onTap: () async {
                                  bool download = await showDownloadDialg(
                                      context,
                                      appLocale
                                          .translate('download-union-list'));
                                  if (download) {
                                    _opendPdf(
                                        index: 3,
                                        context: context,
                                        link: snapshot.data!.appUnionList!,
                                        fileName: 'Union-List');
                                  }
                                },
                              ),
                              buildGridCircle(
                                index: 4,
                                title:
                                    applocale!.translate('organizational-Chart'),
                                color: Color(0xffe8683e),
                                onTap: () async {
                                  bool download = await showDownloadDialg(
                                      context,
                                      applocale!.translate(
                                          'download-organizational-Chart'));
                                  if (download) {
                                    _opendPdf(
                                        index: 4,
                                        context: context,
                                        link: snapshot.data!.organizationalChart!,
                                        fileName: 'Organizational-Chart');
                                  }
                                },
                              ),
                              buildGridCircle(
                                index: 5,
                                title: applocale!.translate('user-manual'),
                                color: Color(0xff006aac),
                                onTap: () async {
                                  bool download = await showDownloadDialg(
                                      context,
                                      appLocale
                                          .translate('download-User-manual'));
                                  if (download) {
                                    _opendPdf(
                                      index: 5,
                                      context: context,
                                      link: snapshot.data!.userManual!,
                                      fileName: 'User-Manual',
                                    );
                                  }
                                },
                              ),
                              buildGridCircle(
                                index: 6,
                                title: appLocale
                                    .translate('federation-committees'),
                                color: Color(0xff000000),
                                onTap: () async {
                                  bool download = await showDownloadDialg(
                                      context,
                                      applocale!.translate(
                                          'download-federation-committees'));
                                  if (download) {
                                    _opendPdf(
                                        index: 6,
                                        context: context,
                                        link: snapshot
                                            .data!.appFederationCommittees!,
                                        fileName: 'Federation-Committees');
                                  }
                                },
                              ),
                              buildGridCircle(
                                index: 7,
                                title: applocale!.translate('events-volunteers'),
                                color: Color(0xffb115ea),
                                onTap: () {
                                  GoTo.screen(context, EventsVolunteers());
                                },
                              ),
                            ],
                          )
                        ],
                      ),
                    );
        },
      ),
    );
  }

  Widget buildGridCircle(
      {String? title, Color? color, Function? onTap, int? index}) {
    return GestureDetector(
      onTap: ()=>onTap!,
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil.portrait! ? 5 : 12, vertical: 2),
        width: 78,
        height: 78,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
        child: index == _downloadingFileIndex
            ? Center(child: CircularProgressIndicator())
            : Text(
                title!,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 10,
                  fontWeight: FontWeight.bold,
                  color: color == Colors.white ? Colors.black : Colors.white,
                  height: 1.5,
                ),
              ),
      ),
    );
  }

  Container buildImageSlider(List<HomeBanner> banners) {
    return Container(
      child: Column(
        children: [
          CarouselSlider.builder(
            itemCount: banners.length,
            options: CarouselOptions(
              aspectRatio: 2.8,
              // autoPlay: true,
              enlargeCenterPage: true,
              onPageChanged: (index, reason) {
                setState(() {
                  _currentImage = index;
                });
              },
            ),
            itemBuilder: (context, index) {
              return ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: banners[index].type == 'image'
                    ? Image.network(
                        banners[index].file!,
                        fit: BoxFit.cover,
                        width: double.infinity,
                      )
                    : GestureDetector(
                        onTap: () => GoTo.screen(
                            context, ShowVideo(videoURL: banners[index].file)),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Image.network(
                              banners[index].image!,
                              fit: BoxFit.cover,
                              width: double.infinity,
                            ),
                            Icon(
                              Icons.play_circle_filled,
                              color: AppColors.secondaryColor,
                              size: 40,
                            )
                          ],
                        ),
                      ),
              );
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: banners.map((b) {
              int index = banners.indexOf(b);
              return Container(
                width: 8.0,
                height: 8.0,
                margin:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 3.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _currentImage == index
                      ? AppColors.accentColor
                      : Color(0xffd7d9df),
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  Future showDownloadDialg(BuildContext context, String msg) {
    var appLocale = AppLocalizations.of(context);
    return UI.dialog(
        accept: true,
        acceptMsg: applocale!.translate('download'),
        cancelMsg: applocale!.translate('cancel'),
        context: context,
        title: msg);
  }

  Future<void> _opendPdf(
      {BuildContext? context, String? fileName, String? link, int? index}) async {
    setState(() {
      _downloadingFileIndex = index;
    });

    File pdfFile = await _localFile(fileName!);
    final String filePath = pdfFile.path;

    // check if file exist
    bool isExist = await pdfFile.exists();

    // if file is exist delete file and download it again.
    if (isExist) await pdfFile.delete();

    final stream = await fetchPost(link!);
    // if no error
    if (stream != null) {
      await pdfFile.writeAsBytes(stream);
      GoTo.screen(context!, PdfScreen(path: filePath, title: fileName));
    } else {
      UI.dialog(
          context: context!,
          title: AppLocalizations.of(context).translate('failed'),
          msg: AppLocalizations.of(context).translate('some-thing-wrong'));
    }

    setState(() {
      _downloadingFileIndex = null;
    });
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> _localFile(String name) async {
    final path = await _localPath;
    return File('$path/$name.pdf');
  }

  Future<Uint8List?> fetchPost(String link) async {
    try {
      var request = await HttpClient().getUrl(Uri.parse(link));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      return bytes;
    } catch (e) {
      print(e);
      return null;
    }
  }
}

class PdfScreen extends StatelessWidget {
  final String? path;
  final String? title;

  PdfScreen({@required this.path, @required this.title});

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      appBar: AppBar(title: Text(title!)),
      path: path,
    );
  }
}
