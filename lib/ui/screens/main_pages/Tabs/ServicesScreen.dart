import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/ui/screens/service_tab_details/companies_services/companies_servecies.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../auth/authentication_service.dart';
import '../../../../core/utils/AssetsRoutes.dart';
import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/text_style.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../service_tab_details/ceo_services/ceo_services.dart';
import '../../service_tab_details/manager_services/manager_services.dart';
import '../../service_tab_details/sevice_tab_details.dart';

class ServicesScreen extends StatefulWidget {
  @override
  _ServicesScreenState createState() => _ServicesScreenState();
}

class _ServicesScreenState extends State<ServicesScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var appLocale = AppLocalizations.of(context);

    return Container(
      color: AppColors.primaryColor,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight!),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            color: AppColors.primaryColor,
            height: 85,
            child: AppHeader(),
          ),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(gradient: AppColors.backgroundGradient),
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    Text(
                      appLocale!.translate('services'),
                      style: KTabHeaderTitle,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: GridView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: ScreenUtil.portrait! ? 2 : 3,
                            crossAxisSpacing: 12,
                            mainAxisSpacing: 12,
                            childAspectRatio: 1.04),
                        children: [
                          itemOption(
                            icon: AssetsRoutes.player,
                            text: appLocale!.translate('player'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: ServiceTabDetails(
                                      title: appLocale!.translate('player')),
                                  category: 'player',
                                  accessError:
                                      appLocale!.translate('only_players'));
                              // GoTo.screen(context, ServiceTabDetails());
                            },
                          ),
                          itemOption(
                            icon: AssetsRoutes.coach,
                            text: appLocale!.translate('coach'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: ServiceTabDetails(
                                      title: appLocale!.translate('coach')),
                                  category: 'coach',
                                  accessError:
                                      appLocale!.translate('only_coaches'));
                            },
                          ),
                          itemOption(
                            icon: AssetsRoutes.rules,
                            text: appLocale!.translate('referee'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: ServiceTabDetails(
                                      title: appLocale!.translate('referee')),
                                  category: 'referee',
                                  accessError:
                                      appLocale!.translate('only_referee'));
                            },
                          ),
                          itemOption(
                            icon: AssetsRoutes.manager,
                            text: appLocale!.translate('manager'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: ManagerServices(),
                                  category: 'manger',
                                  accessError:
                                      appLocale!.translate('only_managers'));
                            },
                          ),
                          itemOption(
                            icon: AssetsRoutes.ceo,
                            text: appLocale
                                !.translate('management-board-member-grid'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: CeoServices(),
                                  category: 'mbd',
                                  accessError: appLocale!.translate('only_mbm'));
                            },
                          ),
                          itemOption(
                            icon: AssetsRoutes.company,
                            text: appLocale!.translate('strategic-company'),
                            onClick: () {
                              itemSelected(
                                  context: context,
                                  screen: CompaniesServices(),
                                  category: 'strategic_companies',
                                  accessError: appLocale
                                      !.translate('only_strategic-companies'));
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget itemOption({String? icon, String? text, Function? onClick}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: InkWell(
          onTap:()=> onClick,
          child: Container(
            color: AppColors.primaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(icon!, height: 45),
                SizedBox(height: 8),
                Text(
                  text!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      height: 1.25,
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void itemSelected(
      {BuildContext? context,
      Widget? screen,
      String? category,
      String? accessError}) {
    var auth = Provider.of<AuthenticationService>(context!, listen: false);
    var usercat = auth.user?.category;

    if (auth.userLoged) {
      if (usercat == category || usercat == 'admin') {
        GoTo.screen(context!, screen);
      } else {
        UI.dialog(context: context!, msg: accessError!);
      }
    } else {
      UI.dialog(
        context: context!,
        msg: AppLocalizations.of(context!)!.translate('only-member-message'),
      );
    }
  }
}
