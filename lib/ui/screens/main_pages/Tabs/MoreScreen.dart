import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/core/utils/screen_util.dart';
import 'package:UAE_KF/ui/screens/about_us/about_us.dart';
import 'package:UAE_KF/ui/screens/more_change_pass/more_change_pass_screen.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

import '../../../../auth/authentication_service.dart';
import '../../../../core/utils/GoTo.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/text_style.dart';
import '../../../shared/ui.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../auth_pages/login_screen.dart';
import '../../change_language/change_language_screen.dart';
import '../../contact_us/contact_us.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    final auth = Provider.of<AuthenticationService>(context, listen: false);
    var appLocale = AppLocalizations.of(context);

    return Container(
      color: AppColors.primaryColor,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight!),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
              color: AppColors.primaryColor, height: 85, child: AppHeader()),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(gradient: AppColors.backgroundGradient),
              child: SingleChildScrollView(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Column(
                  children: [
                    Text(
                      applocale!.translate('more'),
                      style: KTabHeaderTitle,
                    ),
                    SizedBox(height: 15),

                    // Contact us
                    buildListTile(applocale!.translate('contact-us'), onTab: () {
                      GoTo.screen(context, ContactUs());
                    }),

                    buildDivider(),

                    // about us
                    buildListTile(applocale!.translate('about-us'), onTab: () {
                      GoTo.screen(context, AboutUs());
                    }),

                    buildDivider(),

                    // change language
                    buildListTile(applocale!.translate('change-language'),
                        onTab: () {
                      GoTo.screen(context, ChangeLanguageScreen());
                    }),

                    buildDivider(),

                    // if logedin user,  show change password and logout
                    if (auth.userLoged) ...[
                      // change password
                      buildListTile(applocale!.translate('change-password'),
                          onTab: () {
                        GoTo.screen(context, MoreChangePassScreen());
                      }),

                      buildDivider(),

                      // log out
                      buildListTile(
                        applocale!.translate('log-out'),
                        color: Colors.red,
                        icon: Icons.exit_to_app,
                        iconSize: 24,
                        onTab: logOut,
                      ),
                    ],

                    // if not user show login button
                    if (!auth.userLoged)
                      Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 10),
                        child: ButtonWithGradient(
                          title: applocale!.translate('sign-in'),
                          onTap: () {
                            GoTo.screen(context, LoginScreen());
                          },
                        ),
                      )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  logOut() async {
    final auth = Provider.of<AuthenticationService>(context, listen: false);
    try {
      await auth.signOut;
      if (auth.user != null) {
        UI.dialog(
          context: context,
          title: AppLocalizations.of(context).translate('failed'),
          msg: AppLocalizations.of(context).translate('some-thing-wrong'),
        );
      } else {
        GoTo.screenAndRemoveUntil(context, LoginScreen());
      }
    } catch (e) {
      Logger().e(e);
    }
  }

  Widget buildDivider() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Divider(
        color: Colors.white,
        thickness: 0.6,
        height: 0,
      ),
    );
  }

  ListTile buildListTile(
    String text, {
    required Function onTab,
    IconData icon = Icons.arrow_forward_ios,
    Color color = Colors.white,
    double iconSize = 20,
  }) {
    return ListTile(
      onTap: ()=>onTab,
      contentPadding: EdgeInsets.zero,
      dense: true,
      title: Text(text, style: TextStyle(fontSize: 14, color: color)),
      trailing: Icon(icon, color: color, size: iconSize),
    );
  }
}
