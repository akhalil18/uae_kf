import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../auth/authentication_service.dart';
import '../../../../core/utils/GoTo.dart';
import '../../../../core/utils/screen_util.dart';
import '../../../../models/user.dart';
import '../../../shared/styles/app_colors.dart';
import '../../../shared/styles/sized_box.dart';
import '../../../shared/styles/text_style.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/button_with_gradient.dart';
import '../../../widgets/white_button.dart';
import '../../auth_pages/login_screen.dart';
import '../../auth_pages/sign_up_screen.dart';
import '../../edit_profile/edit_profile_screen.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);
    var auth = Provider.of<AuthenticationService>(context, listen: false);

    return Container(
      color: AppColors.primaryColor,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight!),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            color: AppColors.primaryColor,
            height: 85,
            child: AppHeader(),
          ),
          Expanded(
            child: auth.userLoged
                ? authProfile(auth.user!, context)
                : noAuthProfile(context),
          ),
        ],
      ),
    );
  }

  Container authProfile(User user, BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(gradient: AppColors.backgroundGradient),
      child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 33),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                applocale!.translate('profile'),
                style: KTabHeaderTitle,
              ),
            ),
            KMediumSizedBoxHeight,
            Row(
              children: [
                CircleAvatar(
                  radius: 27.5,
                  backgroundColor: Color(0xff59c5c2c9),
                  backgroundImage: NetworkImage(user.image!),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Text(
                    user.name!,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            KLargeSizedBoxHeight,

            // id
            Text(applocale!.translate('identity-passport'),
                style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            dataContainer(user.identity!),

            KMediumSizedBoxHeight,

            // phone
            Text(applocale!.translate('phone-number'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            dataContainer(user.phone!),

            KMediumSizedBoxHeight,

            // email
            Text(applocale!.translate('email'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            dataContainer(user.email!),

            KMediumSizedBoxHeight,

            // gender
            Text(applocale!.translate('gender'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            dataContainer(user.gender!.isNotEmpty
                ? applocale!.translate(user.gender!)
                : user.gender!),

            KLargeSizedBoxHeight,

            Align(
              alignment: Alignment.center,
              child: ButtonWithGradient(
                width: ScreenUtil.screenWidthDp!,
                title: applocale!.translate('edit-profile'),
                onTap: () {
                  GoTo.screen(context, EditProfileScreen(currentUser: user));
                },
              ),
            ),
            KSmallSizedBoxHeight
          ],
        ),
      ),
    );
  }

  Container noAuthProfile(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(gradient: AppColors.backgroundGradient),
      child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                applocale!.translate('profile'),
                style: KTabHeaderTitle,
              ),
            ),
            KLargeSizedBoxHeight,
            Text(
              applocale!.translate('please-login'),
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: Colors.white),
            ),
            KLargeSizedBoxHeight,
            ButtonWithGradient(
              onTap: () {
                GoTo.screen(context, LoginScreen());
              },
              title: applocale!.translate('sign-in'),
            ),
            KLargeSizedBoxHeight,
            WhiteButton(
              title: applocale!.translate('new-account'),
              onTab: () {
                GoTo.screen(context, SignUpScreen());
              },
            ),
          ],
        ),
      ),
    );
  }

  Container dataContainer(String text) {
    return Container(
      alignment: AlignmentDirectional.centerStart,
      padding: const EdgeInsets.symmetric(horizontal: 18),
      width: double.infinity,
      height: 45,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Text(
        text,
        style: TextStyle(
          color: AppColors.greyText,
          fontSize: 14.0,
        ),
      ),
    );
  }
}
