import 'package:UAE_KF/controller/aboutUsController.dart';
import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/ui/widgets/error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../widgets/app_header.dart';
import '../../widgets/title_with_back_button.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: FutureBuilder(
                  future: AboutUsController().getAboutUsDate(),
                  builder: (_, AsyncSnapshot<String> snapshot) {
                    return snapshot.connectionState != ConnectionState.done
                        ? Center(child: CircularProgressIndicator())
                        : !snapshot.hasData
                            ? AppErrorWidget(textColor: Colors.white)
                            : SingleChildScrollView(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Column(
                                  children: [
                                    TitleWithBackButton(
                                        AppLocalizations.of(context)
                                            !.translate('about-union')),
                                    KMediumSizedBoxHeight,
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Html(
                                        data: snapshot.data!,

                                      ),

                                      // Text(
                                      //   snapshot.data,
                                      //   textAlign: TextAlign.center,
                                      //   style: TextStyle(
                                      //     fontSize: 16.0,
                                      //     color: Color(0xffd8d8d8),
                                      //   ),
                                      // ),
                                    )
                                  ],
                                ),
                              );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
