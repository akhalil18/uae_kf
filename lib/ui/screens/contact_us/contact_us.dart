import 'package:UAE_KF/controller/contactUsController.dart';
import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:UAE_KF/core/utils/validator.dart';
import 'package:UAE_KF/ui/shared/ui.dart';
import 'package:flutter/material.dart';

import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../widgets/app_header.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/title_with_back_button.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> with Validator {
  var _formKey = GlobalKey<FormState>();

  var _name = TextEditingController();
  var _email = TextEditingController();
  var _message = TextEditingController();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              alignment: Alignment.center,
              color: AppColors.primaryColor,
              height: 85,
              child: AppHeader(),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration:
                    BoxDecoration(gradient: AppColors.backgroundGradient),
                child: SingleChildScrollView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithBackButton(
                          AppLocalizations.of(context)!.translate('contact-us')),

                      // body
                      buildBody(context),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // name
            Text(appLocale!.translate('name'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _name,
              hint: appLocale!.translate('your-name'),
              validator: (value) => namelValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // email
            Text(appLocale!.translate('email'), style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _email,
              hint: appLocale!.translate('email'),
              keyboardType: TextInputType.emailAddress,
              validator: (value) => emailValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // complain
            Text(appLocale!.translate('message-content'),
                style: KProfileFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _message,
              lines: 3,
              keyboardType: TextInputType.multiline,
              validator: (value) => messageValidator(value!, context),
            ),

            KLargeSizedBoxHeight,

            // send
            ButtonWithGradient(
              busy: isLoading,
              onTap: () {
                _send(
                    context: context,
                    email: _email.text,
                    message: _message.text,
                    name: _name.text);
              },
              title: appLocale!.translate('send'),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _send(
      {String? name, String? email, String? message, BuildContext? context}) async {
    var appLocale = AppLocalizations.of(context!);

    var isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    setState(() {
      isLoading = true;
    });
    final statue = await ContactUsController()
        .contactUs(name: name, email: email, message: message);
    if (statue) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('success'),
        titleColor: Colors.green,
        msg: appLocale!.translate('message-sent-successfully'),
        child: Icon(Icons.check_circle_outline, color: Colors.green, size: 35),
      );
      clearData();
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('not-sent'),
        msg: appLocale!.translate('try-again-message'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    }
    setState(() {
      isLoading = false;
    });
  }

  void clearData() {
    setState(() {
      _name.clear();
      _email.clear();
      _message.clear();
    });
  }
}
