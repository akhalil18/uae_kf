import 'package:flutter/material.dart';

import '../../../controller/reset_password_controller.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/header_with_image.dart';
import '../../widgets/password_text_form_field.dart';
import '../../widgets/transparent_button.dart';
import 'login_screen.dart';

class ChangePasswordScreen extends StatefulWidget {
  final String email;

  ChangePasswordScreen(this.email);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen>
    with Validator {
  final _formKey = GlobalKey<FormState>();
  var _password = TextEditingController();
  var _confirmPassword = TextEditingController();
  var isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            HeaderWithImage('assets/images/img5.png'),
            buildBody(context),
          ],
        ),
      ),
    );
  }

  Container buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appLocale!.translate('change-password'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),

            KMediumSizedBoxHeight,

            // password
            Text(appLocale!.translate('new-password'), style: KTextFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _password,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('new-password'),
              validator: (value) => passwordValidator(value!, context)!,
            ),

            KMediumSizedBoxHeight,

            // Confirm pass
            Text(appLocale!.translate('confirm-password'),
                style: KTextFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _confirmPassword,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('confirm-password'),
            ),

            KLargeSizedBoxHeight,

            // save and go to login buttons
            footerButtons(context),
          ],
        ),
      ),
    );
  }

  Container footerButtons(BuildContext context) {
    return Container(
      width: ScreenUtil.screenWidthDp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ButtonWithGradient(
            busy: isLoading,
            title: AppLocalizations.of(context)!.translate('save'),
            onTap: () {
              changePassword(
                  context: context,
                  password: _password.text,
                  confirmPassword: _confirmPassword.text,
                  email: widget.email);
            },
          ),
          SizedBox(height: 15),
          TransparentButton(
            title: AppLocalizations.of(context)!.translate('back-to-login'),
            onTab: () {
              GoTo.screenAndRemoveUntil(context, LoginScreen());
            },
          ),
        ],
      ),
    );
  }

  changePassword({
    BuildContext? context,
    String? email,
    String? password,
    String? confirmPassword,
  }) async {
    var appLocale = AppLocalizations.of(context!);

    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    } else if (password != confirmPassword) {
      UI.dialog(
          context: context, msg: appLocale!.translate("passwords-don't-match"));
    } else {
      setState(() {
        isLoading = true;
      });
      final statue =
          await ResetPasswordController().resetPassword(email!, password!);
      if (statue) {
        UI.dialog(
          context: context,
          title: appLocale!.translate('success'),
          titleColor: Colors.green,
          msg: appLocale!.translate('password-changed-successfully'),
          child:
              Icon(Icons.check_circle_outline, color: Colors.green, size: 35),
        );
        clearData();
      } else {
        UI.dialog(
          context: context,
          title: appLocale!.translate('failed'),
          msg: appLocale!.translate('try-again-message'),
          child: Icon(Icons.error_outline, color: Colors.red, size: 35),
        );
      }
      setState(() {
        isLoading = false;
      });
    }
  }

  void clearData() {
    setState(() {
      _password.clear();
      _confirmPassword.clear();
    });
  }
}
