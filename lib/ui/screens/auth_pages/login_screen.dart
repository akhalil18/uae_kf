import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

import '../../../controller/login_controller.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/header_with_image.dart';
import '../../widgets/password_text_form_field.dart';
import '../../widgets/text_with_action.dart';
import '../../widgets/transparent_button.dart';
import '../main_pages/main_screen.dart';
import 'reset_password_screen.dart';
import 'sign_up_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Validator {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            HeaderWithImage('assets/images/img2.png', canPop: false),
            buildBody(context),
          ],
        ),
      ),
    );
  }

  Container buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appLocale!.translate('sign-in'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            KMediumSizedBoxHeight,
            Text(
              appLocale!.translate('email'),
              style: KTextFieldHint,
            ),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _emailController,
              icon: Icon(Icons.email),
              hint: appLocale!.translate('email'),
              keyboardType: TextInputType.emailAddress,
              validator: (value) => emailValidator(value!, context)!,
            ),
            KMediumSizedBoxHeight,
            Text(
              appLocale!.translate('password'),
              style: KTextFieldHint,
            ),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _passwordController,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('password'),
              validator: (value) => passwordValidator(value!, context)!,
            ),
            KMediumSizedBoxHeight,
            Align(
              alignment: Alignment.center,
              child: TextWithAction(
                title: appLocale!.translate('forget-pass'),
                onTab: () {
                  // Navigator.of(context).pushNamed(ResetPasswordScreenRoute);
                  GoTo.screen(context, ResetPasswordScreen());
                },
              ),
            ),
            SizedBox(height: 15),

            // loginButton, create account and visitor button
            footerButtons(context),
          ],
        ),
      ),
    );
  }

  Container footerButtons(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Container(
      width: ScreenUtil.screenWidthDp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ButtonWithGradient(
            busy: isLoading,
            title: appLocale!.translate('log-in'),
            onTap: () => login(
                context: context,
                email: _emailController.text.trim(),
                password: _passwordController.text),
          ),
          SizedBox(height: 15),
          TransparentButton(
            title: appLocale!.translate('create-account'),
            onTab: () {
              GoTo.screen(context, SignUpScreen());
            },
          ),
          SizedBox(height: 15),
          TextWithAction(
            title: appLocale!.translate('login-as-visitor'),
            fontColor: AppColors.accentColor,
            fontSize: 16.0,
            onTab: () {
              GoTo.screenAndRemoveUntil(context, MainScreen());
            },
          ),
        ],
      ),
    );
  }

  Future<void> login(
      {BuildContext? context, String? email, String? password}) async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    } else {
      // try to login
      setState(() {
        isLoading = true;
      });

      bool statue = await LogInController()
          .login(context: context, email: email, password: password);
      if (statue) {
        setState(() {
          isLoading = false;
        });
        GoTo.screenAndRemoveUntil(context!, MainScreen());
      } else {
        setState(() {
          isLoading = false;
        });
        UI.dialog(
          context: context!,
          title: AppLocalizations.of(context)!.translate('failed'),
          msg: AppLocalizations.of(context)!.translate('incorrect-mail-pass'),
          child: Icon(Icons.error_outline, color: Colors.red, size: 35),
        );
      }
    }
  }
}
