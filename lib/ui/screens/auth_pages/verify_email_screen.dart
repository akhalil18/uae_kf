import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../controller/SignUpController.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/focus_widget.dart';
import '../../widgets/header_with_image.dart';
import '../main_pages/main_screen.dart';

class VerifyEmailScreen extends StatefulWidget {
  final int? sentCode;

  final String? name;
  final String? category;
  final String? email;
  final String? password;
  final String? phone;
  final String? gender;
  final String? identity;
  final String? deviceToken;
  final File? image;

  const VerifyEmailScreen(
      {this.email,
      this.name,
      this.category,
      this.password,
      this.phone,
      this.gender,
      this.identity,
      this.deviceToken,
      this.image,
      this.sentCode});

  @override
  _VerifyEmailScreenState createState() => _VerifyEmailScreenState();
}

class _VerifyEmailScreenState extends State<VerifyEmailScreen> {
  final _pinController = TextEditingController();
  String? pinCode;
  bool _isloading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: FocusWidget(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderWithImage('assets/images/img4.png'),
              body(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget body(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            appLocale!.translate('verify-email'),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          KMediumSizedBoxHeight,
          Text(
            appLocale!.translate('enter-code-message'),
            style: TextStyle(
              color: Color(0xff68686b),
              fontSize: 12.0,
            ),
          ),
          KLargeSizedBoxHeight,
          pinCodeTextField(context),
          KLargeSizedBoxHeight,
          Align(
            alignment: Alignment.center,
            child: ButtonWithGradient(
              busy: _isloading,
              title: appLocale!.translate('check'),
              onTap: () {
                checkCode(context, pinCode!);
              },
            ),
          ),
        ],
      ),
    );
  }

  PinCodeTextField pinCodeTextField(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return PinCodeTextField(
      keyboardType: TextInputType.number,
      appContext: context,
      length: 4,
      dialogConfig: DialogConfig(
        dialogTitle: appLocale!.translate('past'),
        dialogContent: appLocale!.translate('past-message'),
        affirmativeText: appLocale!.translate('yes'),
        negativeText: appLocale!.translate('cancel'),
      ),
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(8),
        fieldHeight: 50,
        fieldWidth: 45,
        activeFillColor: Colors.white,
        inactiveColor: Colors.grey,
      ),
      backgroundColor: Colors.transparent,
      animationType: AnimationType.fade,
      controller: _pinController,
      autoFocus: false,
      onChanged: (value) {
        pinCode = value;
      },
    );
  }

  checkCode(BuildContext context, String code) async {
    var appLocale = AppLocalizations.of(context);

    if (widget.sentCode.toString() != code) {
      UI.dialog(
        context: context,
        title: appLocale!.translate('failed'),
        msg: appLocale!.translate('wrong-code'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    } else {
      setState(() {
        _isloading = true;
      });
      final statue = await SignUpController().checkActiveCode(
          context: context,
          category: widget.category,
          deviceToken: widget.deviceToken,
          email: widget.email,
          gender: widget.gender,
          identity: widget.identity,
          image: widget.image,
          name: widget.name,
          password: widget.password,
          phone: widget.phone);
      setState(() {
        _isloading = false;
      });

      if (statue) {
        GoTo.screenAndRemoveUntil(context, MainScreen());
      } else {
        UI.dialog(
          context: context,
          title: appLocale!.translate('failed'),
          msg: appLocale!.translate('registration-failed'),
          child: Icon(Icons.error_outline, color: Colors.red, size: 35),
        );
      }
    }
  }
}
