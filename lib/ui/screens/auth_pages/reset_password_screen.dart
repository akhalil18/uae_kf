import 'package:flutter/material.dart';

import '../../../controller/send_reset_code_controller.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/enums.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/focus_widget.dart';
import '../../widgets/header_with_image.dart';
import 'code_verify_screen.dart';

class ResetPasswordScreen extends StatefulWidget {
  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen>
    with Validator {
  final _formKey = GlobalKey<FormState>();
  var _emailController = TextEditingController();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: FocusWidget(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderWithImage('assets/images/img3.png'),
              buildBody(context),
            ],
          ),
        ),
      ),
    );
  }

  Container buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appLocale!.translate('reset-pass'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10),
            Text(
              appLocale!.translate('reset-pass-message'),
              style: TextStyle(
                color: Color(0xff68686b),
                fontSize: 12.0,
              ),
            ),
            SizedBox(height: 15),
            Text(
              appLocale!.translate('email'),
              style: TextStyle(
                color: AppColors.greyText,
                fontSize: 12.0,
              ),
            ),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _emailController,
              icon: Icon(Icons.email),
              hint: appLocale!.translate('email'),
              keyboardType: TextInputType.emailAddress,
              validator: (value) => emailValidator(value!, context)!,
            ),
            KLargeSizedBoxHeight,
            Align(
              alignment: Alignment.center,
              child: ButtonWithGradient(
                busy: _isLoading,
                title: appLocale!.translate('send-code'),
                onTap: () {
                  sendCode(context, _emailController.text, ResetCode.send);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> sendCode(
      BuildContext context, String email, ResetCode resetCodeType) async {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    }
    setState(() {
      _isLoading = true;
    });

    bool sent = await SendResetCodeController().sendCode(email, resetCodeType);

    if (sent) {
      GoTo.screen(context, CodeVerifyScreen(email: email));
    } else {
      UI.dialog(
        context: context,
        title: AppLocalizations.of(context)!.translate('failed'),
        msg: AppLocalizations.of(context)!.translate('email-not-exist'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    }
    setState(() {
      _isLoading = false;
    });
  }
}
