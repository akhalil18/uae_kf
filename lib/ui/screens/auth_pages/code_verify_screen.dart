import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../controller/check_reset_code_controller.dart';
import '../../../controller/send_reset_code_controller.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/enums.dart';
import '../../../core/utils/screen_util.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/focus_widget.dart';
import '../../widgets/header_with_image.dart';
import 'change_password_screen.dart';

class CodeVerifyScreen extends StatefulWidget {
  final String? email;

  const CodeVerifyScreen({this.email});
  @override
  _CodeVerifyScreenState createState() => _CodeVerifyScreenState();
}

class _CodeVerifyScreenState extends State<CodeVerifyScreen> {
  final _pinController = TextEditingController();
  String? pinCode;

  bool _isloading = false;
  bool _resendingCode = false;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: FocusWidget(
        child: SingleChildScrollView(
          child: Column(
            children: [
              HeaderWithImage('assets/images/img4.png'),
              body(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget body(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            appLocale!.translate('enter-code'),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          KMediumSizedBoxHeight,
          Text(
            appLocale!.translate('enter-code-message'),
            style: TextStyle(
              color: Color(0xff68686b),
              fontSize: 12.0,
            ),
          ),
          KLargeSizedBoxHeight,
          pinCodeTextField(context),
          KLargeSizedBoxHeight,
          Align(
            alignment: Alignment.center,
            child: ButtonWithGradient(
              busy: _isloading,
              title: appLocale!.translate('check'),
              onTap: () {
                checkCode(context, widget.email!, pinCode!);
              },
            ),
          ),
          KMediumSizedBoxHeight,
          resendCode(context),
        ],
      ),
    );
  }

  PinCodeTextField pinCodeTextField(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return PinCodeTextField(
      keyboardType: TextInputType.number,
      appContext: context,
      length: 4,
      dialogConfig: DialogConfig(
        dialogTitle: appLocale!.translate('past'),
        dialogContent: appLocale!.translate('past-message'),
        affirmativeText: appLocale!.translate('yes'),
        negativeText: appLocale!.translate('cancel'),
      ),
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(8),
        fieldHeight: 50,
        fieldWidth: 45,
        activeFillColor: Colors.white,
        inactiveColor: Colors.grey,
      ),
      backgroundColor: Colors.transparent,
      animationType: AnimationType.fade,
      controller: _pinController,
      autoFocus: false,
      onChanged: (value) {
        pinCode = value;
      },
    );
  }

  Row resendCode(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if (!_resendingCode)
          IconButton(
            icon: Icon(Icons.refresh),
            color: AppColors.accentColor,
            splashRadius: 20.0,
            iconSize: 25.0,
            onPressed: () {
              resend(context, widget.email!, ResetCode.resend);
            },
          ),
        Text(
          appLocale!.translate('resend-code'),
          style: TextStyle(fontSize: 13.0, color: AppColors.greyText),
        )
      ],
    );
  }

  checkCode(BuildContext context, String email, String code) async {
    var appLocale = AppLocalizations.of(context);

    setState(() {
      _isloading = true;
    });
    final statue = await CheckResetCodeController().checkResetCode(email, code);
    setState(() {
      _isloading = false;
    });

    if (statue) {
      GoTo.screen(context, ChangePasswordScreen(email));
    } else {
      UI.dialog(
        context: context,
        title: appLocale!.translate('failed'),
        msg: appLocale!.translate('wrong-code'),
        child: Icon(Icons.error_outline, color: Colors.red, size: 35),
      );
    }
  }

  Future<void> resend(
      BuildContext context, String email, ResetCode resetCodeType) async {
    setState(() {
      _resendingCode = true;
    });

    await SendResetCodeController().sendCode(email, resetCodeType);

    setState(() {
      _resendingCode = false;
    });
  }
}
