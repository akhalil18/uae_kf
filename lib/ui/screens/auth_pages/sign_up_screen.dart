import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../controller/SignUpController.dart';
import '../../../core/localization/app_localizations.dart';
import '../../../core/utils/AssetsRoutes.dart';
import '../../../core/utils/GoTo.dart';
import '../../../core/utils/dropDownLists.dart';
import '../../../core/utils/screen_util.dart';
import '../../../core/utils/validator.dart';
import '../../shared/styles/app_colors.dart';
import '../../shared/styles/sized_box.dart';
import '../../shared/styles/text_style.dart';
import '../../shared/ui.dart';
import '../../widgets/button_with_gradient.dart';
import '../../widgets/custom_drop_down_button.dart';
import '../../widgets/custom_text_form_field.dart';
import '../../widgets/header_with_image.dart';
import '../../widgets/password_text_form_field.dart';
import 'login_screen.dart';
import 'verify_email_screen.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> with Validator {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _identify = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  File? _selectedImage;
  var _gender;
  var _catrgory;
  // Country _selected;

  bool isLoading = false;

  @override
  void initState() {
    // _selected = Country.findByIsoCode("AE");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Scaffold(
      backgroundColor: AppColors.backgrounColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            HeaderWithImage(
              'assets/images/img6.png',
              height: ScreenUtil.screenHeightDp! * 0.27,
            ),
            buildBody(context)
          ],
        ),
      ),
    );
  }

  Container buildBody(BuildContext context) {
    var appLocale = AppLocalizations.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 33),
      width: ScreenUtil.screenWidthDp,
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              appLocale!.translate('new-account'),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),

            KLargeSizedBoxHeight,

            // Image
            Row(
              children: [
                Stack(
                  overflow: Overflow.visible,
                  children: [
                    GestureDetector(
                      onTap: () => _showImageDialog(context),
                      child: Container(
                        width: 55,
                        height: 55,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.grey),
                          image: _selectedImage == null
                              ? DecorationImage(
                                  image: AssetImage(AssetsRoutes.img_icon))
                              : DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(_selectedImage!)),
                        ),
                      ),
                    ),
                    Positioned(
                      top: -8,
                      child: GestureDetector(
                        onTap: () {
                          setState(() => _selectedImage = null);
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 12,
                          child: Icon(
                            Icons.close,
                            color: Colors.grey,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(width: 10),
                Text(appLocale!.translate('add-profile-image'),
                    style: KTextFieldHint),
              ],
            ),

            KMediumSizedBoxHeight,

            // User name
            Text(appLocale!.translate('user-name'), style: KTextFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _name,
              icon: Icon(Icons.perm_identity),
              hint: appLocale!.translate('user-name'),
              validator: (value) => namelValidator(value!, context)!,
            ),

            KMediumSizedBoxHeight,

            // category
            Text(appLocale!.translate('category'), style: KTextFieldHint),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              validator: (value) => fieldValidator(value, context)!,
              hint: appLocale!.translate('category'),
              items: getCatrgory(context)
                  .map((i) => DropdownMenuItem(
                        child: Text(i.name!),
                        value: i.value,
                      ))
                  .toList(),
              value: _catrgory,
              onChange: (value) {
                setState(() {
                  _catrgory = value;
                });
              },
            ),

            KMediumSizedBoxHeight,

            // identify number
            Row(
              children: [
                Text(appLocale!.translate('identification-number'),
                    style: KTextFieldHint),
                SizedBox(width: 8),
                Text(appLocale!.translate('optional'), style: KTextFieldHint),
              ],
            ),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _identify,
              icon: Icon(Icons.folder_shared),
              hint: appLocale!.translate('identification-number'),
              validator: (value) => null,
            ),

            KMediumSizedBoxHeight,

            // Phone number
            Row(
              children: [
                Text(appLocale!.translate('phone-number'),
                    style: KTextFieldHint),
                SizedBox(width: 8),
                Text(appLocale!.translate('optional'), style: KTextFieldHint),
              ],
            ),
            KSmallSizedBoxHeight,
            phoneField(context),

            KMediumSizedBoxHeight,

            // // Type
            // chooseType(context),
            Row(
              children: [
                Text(appLocale!.translate('gender'), style: KTextFieldHint),
                SizedBox(width: 8),
                Text(appLocale!.translate('optional'), style: KTextFieldHint),
              ],
            ),
            KSmallSizedBoxHeight,
            CustomDropDownButton(
              hint: appLocale!.translate('gender'),
              items: [
                DropdownMenuItem(
                  child: Text(appLocale!.translate('male')),
                  value: 'male',
                ),
                DropdownMenuItem(
                  child: Text(appLocale!.translate('female')),
                  value: 'female',
                ),
                DropdownMenuItem(
                  child: Text(appLocale!.translate("don't-prefer-say")),
                  value: '',
                ),
              ],
              onChange: (value) {
                setState(() {
                  _gender = value;
                  print(_gender);
                });
              },
            ),

            KMediumSizedBoxHeight,

            // Email
            Text(appLocale!.translate('email'), style: KTextFieldHint),
            KSmallSizedBoxHeight,
            CustomTextFormField(
              controller: _email,
              icon: Icon(Icons.email),
              hint: appLocale!.translate('email'),
              keyboardType: TextInputType.emailAddress,
              validator: (value) => emailValidator(value!, context),
            ),

            KMediumSizedBoxHeight,

            // password
            Text(appLocale!.translate('password'), style: KTextFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _password,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('password'),
              validator: (value) => passwordValidator(value!, context)!,
            ),

            KMediumSizedBoxHeight,

            // Confirm pass
            Text(appLocale!.translate('confirm-password'),
                style: KTextFieldHint),
            KSmallSizedBoxHeight,
            PasswordTextFormFiled(
              controller: _confirmPassword,
              icon: Icon(Icons.lock_outline),
              hint: appLocale!.translate('confirm-password'),
            ),

            KLargeSizedBoxHeight,
            footerButtons(context),
            KSmallSizedBoxHeight
          ],
        ),
      ),
    );
  }

  Row chooseType(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Row(
      children: [
        Text(
          appLocale!.translate('gender'),
          style: TextStyle(
            color: Color(0xff17161c),
            fontSize: 12.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(width: 30),
        Radio(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          value: 'male',
          groupValue: _gender,
          onChanged: (value) {
            setState(() {
              _gender = value;
            });
          },
        ),
        Text(
          appLocale!.translate('male'),
          style: TextStyle(
            fontSize: 14.0,
          ),
        ),
        SizedBox(width: 20),
        Radio(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          value: 'female',
          groupValue: _gender,
          onChanged: (value) {
            setState(() {
              _gender = value;
            });
          },
        ),
        Text(
          appLocale!.translate('female'),
          style: TextStyle(
            fontSize: 14.0,
          ),
        ),
      ],
    );
  }

  Container phoneField(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              height: 45,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.black54),
              ),
              child: Text('‎+971'),
              //  CountryPicker(
              //   dialingCodeTextStyle:
              //       SizeandStyleUtills().getTextStyleRegular(fontsize: 15),
              //   dense: false,
              //   showFlag: false,
              //   //displays flag, true by default
              //   showDialingCode: true,
              //   //displays dialing code, false by default
              //   showName: false,
              //   //displays country name, true by default
              //   showCurrency: false,
              //   //eg. 'British pound'
              //   showCurrencyISO: false,
              //   //eg. 'GBP'
              //   selectedCountry: _selected,
              //   onChanged: (Country country) {
              //     setState(() {
              //       _selected = country;
              //     });
              //   },
              // ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            flex: 6,
            child: CustomTextFormField(
              controller: _phone,
              hint: AppLocalizations.of(context)!.translate('phone-number'),
              keyboardType: TextInputType.phone,
              // validator: (value) => mobileValidator(value, context),
            ),
          )
        ],
      ),
    );
  }

  Container footerButtons(BuildContext context) {
    var appLocale = AppLocalizations.of(context);

    return Container(
      width: ScreenUtil.screenWidthDp,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ButtonWithGradient(
            busy: isLoading,
            onTap: () => signUp(
                context: context,
                name: _name.text,
                category: _catrgory,
                email: _email.text,
                password: _password.text,
                confirmedPassword: _confirmPassword.text,
                phone: _phone.text ?? '',
                gender: _gender ?? '',
                identity: _identify.text ?? '',
                deviceToken: 'token',
                image: _selectedImage),
            title: appLocale!.translate('create-account'),
          ),
          SizedBox(height: 15),
          Container(
            width: ScreenUtil.screenWidthDp,
            height: 50,
            child: OutlineButton(
              onPressed: () {
                GoTo.screenAndRemoveUntil(context, LoginScreen());
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              borderSide: BorderSide(color: Color(0xff5f79a4), width: 1.5),
              textColor: Color(0xff5763a9),
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                        text: appLocale!.translate('already-have-account'),
                        style: TextStyle(
                          color: Color(0xff5763a9),
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          fontFamily:
                              Theme.of(context).textTheme.subtitle1!.fontFamily,
                        )),
                    TextSpan(
                        text: ' ' + appLocale!.translate('sign-in'),
                        style: TextStyle(
                          color: AppColors.accentColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          fontFamily:
                              Theme.of(context).textTheme.subtitle1!.fontFamily,
                        )),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _showImageDialog(BuildContext context) async {
    var appLocale = AppLocalizations.of(context);

    return showDialog(
        context: context,
        builder: (ctx) {
          return SimpleDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            title: Text(
              appLocale!.translate('choose-from'),
              style: TextStyle(color: AppColors.accentColor),
            ),
            children: <Widget>[
              SimpleDialogOption(
                child: Text(appLocale!.translate('camera')),
                onPressed: () async {
                  Navigator.of(ctx).pop();
                  pickImage(ImageSource.camera);
                },
              ),
              SimpleDialogOption(
                child: Text(appLocale!.translate('gallery')),
                onPressed: () async {
                  Navigator.of(ctx).pop();
                  pickImage(ImageSource.gallery);
                },
              ),
            ],
          );
        });
  }

  Future<void> pickImage(ImageSource source) async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(
      source: source,
      maxWidth: 960,
      maxHeight: 675,
    );
    if (pickedFile == null) {
      return;
    }
    setState(() {
      _selectedImage = File(pickedFile.path);
    });
  }

  Future<void> signUp({
    BuildContext? context,
    String? name,
    String? category,
    String? email,
    String? password,
    String? confirmedPassword,
    String? phone,
    String? gender,
    String? identity,
    String? deviceToken,
    File? image,
  }) async {
    var appLocale = AppLocalizations.of(context!);

    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    } else if (confirmedPassword != password) {
      UI.dialog(
          context: context, msg: appLocale!.translate("passwords-don't-match"));
    } else {
      // if all fields is valid
      setState(() {
        isLoading = true;
      });

      // check if email exixt
      bool isExist = await SignUpController().checkUserEmail(email!);

      if (isExist == null) {
        UI.dialog(
            context: context, title: appLocale!.translate('incorrect-email'));
      } else if (isExist) {
        UI.dialog(context: context, title: appLocale!.translate('email-exist'));
      } else {
        int code = await SignUpController().signUp(
            category: category,
            name: name,
            email: email,
            password: password,
            phone: phone,
            gender: gender,
            identity: identity,
            deviceToken: deviceToken);

        print("Code::" + code.toString());
        // Go to second page
        if (code != null) {
          GoTo.screen(
              context,
              VerifyEmailScreen(
                sentCode: code,
                category: category!,
                name: name!,
                deviceToken: deviceToken!,
                email: email!,
                gender: gender!,
                identity: identity!,
                image: image!,
                password: password!,
                phone: phone!,
              ));
        } else {
          UI.dialog(
            context: context,
            title: appLocale!.translate('failed'),
            msg: appLocale!.translate('registration-failed'),
            child: Icon(Icons.error_outline, color: Colors.red, size: 35),
          );
        }
      }
      setState(() {
        isLoading = false;
      });
    }
  }
}
