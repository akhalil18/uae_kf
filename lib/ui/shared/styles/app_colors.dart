import 'package:flutter/material.dart';

class AppColors {
  static const Color backgrounColor = Color(0xfff7f7f7);

  // dark blue
  static const Color primaryColor = Color(0xff1e284b);

  // blue color
  static const Color secondaryColor = Color(0xff5967a8);

  static const Color whiteColor = Color(0xffffffff);

  // green color
  static const Color accentColor = Color(0xff229c73);

  static const Color greyText = Color(0xff68686b);

  static Gradient backgroundGradient = LinearGradient(
    colors: [
      Color(0xff5f6683),
      Color(0xff1e284b),
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
