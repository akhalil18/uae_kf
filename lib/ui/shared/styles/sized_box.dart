import 'package:flutter/material.dart';

const SizedBox KSmallSizedBoxHeight = SizedBox(height: 8);
const SizedBox KMediumSizedBoxHeight = SizedBox(height: 10);
const SizedBox KLargeSizedBoxHeight = SizedBox(height: 20);
