import 'package:UAE_KF/ui/shared/styles/app_colors.dart';
import 'package:flutter/material.dart';

const TextStyle KTextFieldHint = TextStyle(
  color: AppColors.greyText,
  fontSize: 12.0,
);
const TextStyle KProfileFieldHint = TextStyle(
  color: AppColors.whiteColor,
  fontSize: 12.0,
);

const TextStyle KTabHeaderTitle =
    TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold);
