/*
 * @desc [class that will hold most of the reusable UI components and operators like textStyles and Dialogs]
 */
import 'dart:async';

import 'package:flutter/material.dart';

class UI {
  static Future<dynamic> dialog(
      {BuildContext? context,
      bool? dismissible = true,
      Widget? child,
      bool? accept,
      String? title,
      Color titleColor = Colors.black54,
      String? msg,
      String? acceptMsg,
      String? cancelMsg}) async {
    return await showDialog<dynamic>(
        context: context!,
        barrierDismissible: dismissible!,
        builder: (context) {
          return Dialog(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (title != null)
                    Text(title,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: titleColor,
                            fontSize: 16)),
                  if (child != null)
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        child: child),
                  if (msg != null)
                    Text(
                      msg,
                      maxLines: 2,
                      style: TextStyle(fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  if (accept != null) ...[
                    SizedBox(height: 10),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          height: 35,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [Color(0xff71b198), Color(0xff5761a9)],
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                              ),
                              borderRadius: BorderRadius.circular(10)),
                          child: FlatButton(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            textColor: Colors.white,
                            child: Text(acceptMsg!),
                            onPressed: () => Navigator.of(context).pop(true),
                          ),
                        ),
                        SizedBox(width: 10),
                        Container(
                          height: 35,
                          child: OutlineButton(
                            textColor: Colors.red,
                            highlightedBorderColor: Colors.black26,
                            borderSide: BorderSide(color: Colors.red),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0)),
                            onPressed: () => Navigator.of(context).pop(false),
                            child: Text(
                              cancelMsg!,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                  ],
                ],
              ),
            ),
          );
        });
  }

  static unfocus(context) {
    FocusScope.of(context).unfocus();
  }

  // static toast(String msg) {
  //   Fluttertoast.showToast(
  //     msg: msg,
  //     fontSize: 15.0,
  //     timeInSecForIos: 1,
  //     textColor: Colors.white,
  //     gravity: ToastGravity.BOTTOM,
  //     toastLength: Toast.LENGTH_SHORT,
  //     backgroundColor: Colors.grey[700],
  //   );
  // }
}
