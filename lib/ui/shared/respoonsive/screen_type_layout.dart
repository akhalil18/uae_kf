import 'package:flutter/material.dart';

import '../../../core/utils/screen_util.dart';
import 'responsive_builder.dart';

class ScreenTypeLayout extends StatelessWidget {
  // Mobile will be returned by default
  final Widget mobile;
  final Widget? tablet;
  final Widget? desktop;

  const ScreenTypeLayout(
      { required this.mobile, this.tablet, this.desktop})
     ;

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      // Check device type
      switch (sizingInformation.deviceType) {
        case DeviceType.Desktop:
          return desktop ?? mobile;
          break;

        case DeviceType.Tablet:
          return tablet ?? mobile;
          break;

        default:
          return mobile;
      }
    });
  }
}
