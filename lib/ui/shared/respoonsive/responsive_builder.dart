import 'package:UAE_KF/models/sizing_information.dart';
import 'package:flutter/material.dart';

import '../../../core/utils/screen_util.dart';

class ResponsiveBuilder extends StatelessWidget {
  final Widget Function(
      BuildContext context, SizingInformation sizingInformation) builder;

  const ResponsiveBuilder({required this.builder});

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (context, boxConstraints) {
        final sizingInformation = SizingInformation(
          screenSize: mediaQuery.size,
          deviceType: ScreenUtil.getDeviceType(mediaQuery),
          localWidgetSize:
              Size(boxConstraints.maxWidth, boxConstraints.maxHeight),
        );

        return builder(context, sizingInformation);
      },
    );
  }
}
