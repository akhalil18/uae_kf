import 'package:flutter/material.dart';

class ButtonWithGradient extends StatelessWidget {
  final String? title;
  final Function? onTap;
  final bool? busy;
  final double? width;

  ButtonWithGradient({this.title, this.onTap, this.busy = false, this.width});
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 600),
      height: 50,
      width: width != null ? width : MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff71b198), Color(0xff5761a9)],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
          borderRadius: BorderRadius.circular(10)),
      child: busy!
          ? Center(child: CircularProgressIndicator())
          : FlatButton(
              child: Text(
                title!,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                    color: Colors.white),
              ),
              onPressed: ()=> busy! ? null : onTap!,
            ),
    );
  }
}
