import 'package:flutter/material.dart';

import '../../core/utils/screen_util.dart';

class LanguageButton extends StatelessWidget {
  final String ?title;
  final Color? color;
  final Function? onTab;

  const LanguageButton({
    this.title,
    this.onTab,
    this.color,
  });
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context);

    return Container(
      height: 55,
      child: RaisedButton(
        onPressed:()=> onTab,
        color: color,
        textColor: Colors.white,
        child: Text(
          title!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: color == Colors.transparent
              ? BorderSide(color: Colors.white)
              : BorderSide.none,
        ),
      ),
    );
  }
}
