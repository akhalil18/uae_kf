 import 'package:flutter/material.dart';

import '../../core/utils/screen_util.dart';
import 'app_header.dart';

class HeaderWithImage extends StatelessWidget {
  final String? image;
  final bool? canPop;
  final double? height;

  HeaderWithImage(this.image, {this.canPop = true, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight! + 10),
      width: ScreenUtil.screenWidthDp,
      height: ScreenUtil.portrait!
          ? height == null ? ScreenUtil.screenHeightDp! * 0.35 : height
          : ScreenUtil.screenHeightDp! * 0.5,
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(image!),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          AppHeader(),

          // Back Button
          if (canPop!)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Container(
                width: ScreenUtil.screenWidthDp,
                alignment: AlignmentDirectional.topStart,
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: Icon(Icons.arrow_back),
                  color: Colors.white,
                  iconSize: 30,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
