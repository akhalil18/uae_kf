import 'package:flutter/material.dart';

class WhiteButton extends StatelessWidget {
  final String? title;
  final Color? color;
  final Function? onTab;
  final double? width;

  const WhiteButton(
      {this.title, this.onTab, this.color = Colors.white, this.width});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width != null ? width : double.infinity,
      height: 50,
      child: RaisedButton(
        onPressed:()=> onTab,
        color: color,
        textColor: Color(0xff5763a9),
        child: Text(
          title!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
