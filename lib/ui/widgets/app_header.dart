import 'package:UAE_KF/core/utils/AssetsRoutes.dart';
import 'package:flutter/material.dart';

class AppHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Stack(
        alignment: Alignment.centerRight,
        children: [
          Image.asset(
            AssetsRoutes.logo,
            width: 65,
            height: 65,
          ),
          Align(
            alignment: Alignment.center,
            child: Text(
              'UAE KF',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
                color: Colors.white,
                fontSize: 30.0,
              ),
            ),
          ),
          // Image.asset(
          //   AssetsRoutes.wkf_logo,
          //   width: 68,
          //   height: 68,
          // ),
        ],
      ),
    );
  }
}
