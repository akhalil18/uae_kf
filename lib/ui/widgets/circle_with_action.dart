import 'package:UAE_KF/core/utils/screen_util.dart';
import 'package:UAE_KF/ui/shared/styles/app_colors.dart';
import 'package:flutter/material.dart';

class CircleWithAction extends StatelessWidget {
  final String? title;
  final Color? color;
  final Function? onTap;
  final double? size;

  const CircleWithAction(
      {this.title, this.color = Colors.white, this.onTap, this.size = 90});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>onTap,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.symmetric(
            horizontal: ScreenUtil.portrait! ? 8 : 12, vertical: 10),
        width: size,
        height: size,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
        child: Text(
          title!,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            color:
                color == Colors.white ? AppColors.primaryColor : Colors.white,
            height: 1.5,
          ),
        ),
      ),
    );
  }
}
