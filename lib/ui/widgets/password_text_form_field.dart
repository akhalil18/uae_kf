import 'package:flutter/material.dart';

class PasswordTextFormFiled extends StatefulWidget {
  final int? lines;
  final bool? secure;
  final String? hint;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final Widget? icon;
  final String? Function(String?)? validator;

  PasswordTextFormFiled(
      {
      this.secure = true,
      this.controller,
      this.hint,
      this.lines = 1,
      this.keyboardType = TextInputType.text,
      this.icon,
      this.validator});

  @override
  _PasswordTextFormFiledState createState() => _PasswordTextFormFiledState();
}

class _PasswordTextFormFiledState extends State<PasswordTextFormFiled> {
  bool? _secure;
  @override
  void initState() {
    super.initState();
    _secure = widget.secure;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: widget.validator,
      maxLines: widget.lines,
      keyboardType: widget.keyboardType,
      obscureText: _secure!,
      controller: widget.controller,
      style: TextStyle(fontSize: 15),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: widget.icon,
        suffixIcon: IconButton(
            icon: _secure! ? Icon(Icons.visibility_off) : Icon(Icons.visibility),
            onPressed: () {
              setState(() => _secure = !_secure!);
            }),
        hintText: widget.hint,
        hintStyle: TextStyle(fontSize: 14),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
