import 'package:UAE_KF/core/localization/app_localizations.dart';
import 'package:flutter/material.dart';

class AppErrorWidget extends StatelessWidget {
  final Color textColor;

  const AppErrorWidget({this.textColor = Colors.black38});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 40,
              backgroundColor: Colors.red[800],
              child: Icon(
                Icons.close,
                color: Colors.white,
                size: 40,
              ),
            ),
            SizedBox(height: 10),
            Text(
              AppLocalizations.of(context)!.translate('ops'),
              textDirection: TextDirection.ltr,
              style: TextStyle(
                color: Colors.red[800],
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w600,
                fontSize: 20,
              ),
            ),
            SizedBox(height: 20),
            Text(
              AppLocalizations.of(context)!.translate('error-message'),
              textDirection: TextDirection.ltr,
              style: TextStyle(
                color: textColor,
                fontFamily: 'Roboto',
                fontSize: 18,
              ),
            )
          ],
        ),
      ),
    );
  }
}
