import 'package:flutter/material.dart';

class TextWithAction extends StatelessWidget {
  final String? title;
  final Function? onTab;
  final double? fontSize;
  final Color? fontColor;

  const TextWithAction({
    this.title,
    this.onTab,
    this.fontSize = 12.0,
    this.fontColor = Colors.black,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:()=> onTab,
      child: Text(
        title!,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: fontSize,
          color: fontColor,
        ),
      ),
    );
  }
}
