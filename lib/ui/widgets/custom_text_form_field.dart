import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final int? lines;
  final bool? secure;
  final String? hint;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final Widget? icon;
  final String? Function(String?)? validator;
  const CustomTextFormField(
      {
      this.secure = false,
      this.controller,
      this.hint,
      this.lines = 1,
      this.keyboardType = TextInputType.text,
      this.icon,
      this.validator});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validator,
      maxLines: lines,
      keyboardType: keyboardType,
      obscureText: secure!,
      controller: controller,
      style: TextStyle(fontSize: 15),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        prefixIcon: icon,
        hintText: hint,
        hintStyle: TextStyle(fontSize: 14),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
