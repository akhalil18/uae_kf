import 'package:flutter/material.dart';

class TransparentButton extends StatelessWidget {
  final String? title;
  final Function? onTab;
  final double? width;

  const TransparentButton({this.title, this.onTab, this.width});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width != null ? width : double.infinity,
      height: 50,
      child: OutlineButton(
        borderSide: BorderSide(color: Color(0xff5f79a4), width: 1.5),
        onPressed: ()=> onTab,
        textColor: Color(0xff5763a9),
        child: Text(
          title!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15.0,
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
    );
  }
}
