import 'package:flutter/material.dart';

class CustomDropDownButton extends StatelessWidget {
  final List<DropdownMenuItem>? items;
  final String? hint;
  final dynamic value;
  final void Function(dynamic)? onChange;
  final String? Function(dynamic)? validator;

  const CustomDropDownButton({
    this.items,
    this.hint,
    this.value,
    this.onChange,
    this.validator,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      style: TextStyle(
          color: Colors.black87,
          fontSize: 14,
          fontFamily: Theme.of(context).textTheme.button!.fontFamily),
      isDense: false,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.symmetric(horizontal: 12),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      validator: validator,
      hint: Text(hint!),
      value: value,
      items: items,
      onChanged: onChange,
    );
  }
}
