class ModelCourse {
  List<Course>? courses;
  bool? status;
  String? message;

  ModelCourse({this.courses, this.status, this.message});

  ModelCourse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      courses = [];
      json['data'].forEach((v) {
        courses!.add(new Course.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.courses != null) {
      data['data'] = this.courses!.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Course {
  int? id;
  String? name;
  String? link;

  Course({this.id, this.name, this.link});

  Course.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['link'] = this.link;
    return data;
  }
}
