class UserResponse {
  User? user;
  bool? status;
  String? message;

  UserResponse({this.user, this.status, this.message});

  UserResponse.fromJson(Map<String, dynamic> json) {
    user = json['data'] != null ? new User.fromJson(json['data']) : null;
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (this.user != null) {
      data['data'] = this.user!.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class User {
  int? id;
  String? name;
  String? email;
  String? phone;
  String? identity;
  String? gender;
  String? category;
  String? image;
  String? jwt;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.identity,
      this.gender,
      this.category,
      this.image,
      this.jwt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    identity = json['identity'];
    gender = json['gender'];
    category = json['category'];
    image = json['image'];
    jwt = json['jwt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['identity'] = this.identity;
    data['gender'] = this.gender;
    data['category'] = this.category;
    data['image'] = this.image;
    data['jwt'] = this.jwt;
    return data;
  }
}
