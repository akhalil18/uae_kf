class ModelChampionships {
  List<Championship>? championships;
  bool? status;
  String? message;

  ModelChampionships({this.championships, this.status, this.message});

  ModelChampionships.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      championships = [];
      json['data'].forEach((v) {
        championships!.add(new Championship.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.championships != null) {
      data['data'] = this.championships!.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Championship {
  int? id;
  String? name;

  Championship({this.id, this.name});

  Championship.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
