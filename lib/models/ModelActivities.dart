class ModelActivities {
  ActivitiesData? data;
  bool? status;
  String? message;

  ModelActivities({this.data, this.status, this.message});

  ModelActivities.fromJson(Map<String, dynamic> json) {
    data =
        json['data'] != null ? new ActivitiesData.fromJson(json['data']) : null;
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class ActivitiesData {
  List<MMedia>? images;
  List<MMedia>? videos;

  ActivitiesData({this.images, this.videos});

  ActivitiesData.fromJson(Map<String, dynamic> json) {
    if (json['images'] != null) {
      images = [];
      json['images'].forEach((v) {
        images!.add(new MMedia.fromJson(v));
      });
    }
    if (json['videos'] != null) {
      videos = [];
      json['videos'].forEach((v) {
        videos!.add(new MMedia.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.images != null) {
      data['images'] = this.images!.map((v) => v.toJson()).toList();
    }
    if (this.videos != null) {
      data['videos'] = this.videos!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MMedia {
  int? id;
  String? file;
  String? image;

  MMedia({this.id, this.file, this.image});

  MMedia.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    file = json['file'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['file'] = this.file;
    data['image'] = this.image;
    return data;
  }
}
