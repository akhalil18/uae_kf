class ChampRegistrationModel {
  String? champName;
  int? champId;
  String? category;
  String? champType;
  String? gender;
  String? weight;

  ChampRegistrationModel(
      {this.champId,
      this.category,
      this.champType,
      this.gender,
      this.weight,
      this.champName});
}

// --form 'championship_id=2' \
// --form 'category=cubs_and_girls' \
// --form 'championship_type=kumite' \
// --form 'kumite_type=men' \
// --form 'kumite_weight=60'
