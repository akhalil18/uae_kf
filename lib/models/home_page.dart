class HomePageResponse {
  HomeData? data;
  bool? status;
  String? message;

  HomePageResponse({this.data, this.status, this.message});

  HomePageResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? HomeData.fromJson(json['data']) : null;
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class HomeData {
  String? poster;
  String? appUnionList;
  String? organizationalChart;
  String? userManual;
  String? appFederationCommittees;
  List<HomeBanner>? banners;

  HomeData(
      {this.poster,
      this.appUnionList,
      this.organizationalChart,
      this.userManual,
      this.appFederationCommittees,
      this.banners});

  HomeData.fromJson(Map<String, dynamic> json) {
    poster = json['poster'];
    appUnionList = json['app_union_list'];
    organizationalChart = json['organizational_chart'];
    userManual = json['user_manual'];
    appFederationCommittees = json['app_federation_committees'];
    if (json['banners'] != null) {
      banners = [];
      json['banners'].forEach((v) {
        banners!.add(HomeBanner.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['poster'] = this.poster;
    data['app_union_list'] = this.appUnionList;
    data['organizational_chart'] = this.organizationalChart;
    data['user_manual'] = this.userManual;
    data['app_federation_committees'] = this.appFederationCommittees;
    if (this.banners != null) {
      data['banners'] = this.banners!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class HomeBanner {
  int? id;
  String? file;
  String? image;
  String? type;

  HomeBanner({this.id, this.file, this.image, this.type});

  HomeBanner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    file = json['file'];
    image = json['image'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['file'] = this.file;
    data['image'] = this.image;
    data['type'] = this.type;
    return data;
  }
}
