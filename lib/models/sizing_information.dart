import 'package:UAE_KF/core/utils/screen_util.dart';
import 'package:flutter/material.dart';

class SizingInformation {
  final DeviceType? deviceType;
  final Size? screenSize;
  final Size? localWidgetSize;

  SizingInformation({
    this.deviceType,
    this.screenSize,
    this.localWidgetSize,
  });

  @override
  String toString() {
    return 'DeviceType:$DeviceType ScreenSize:$screenSize LocalWidgetSize:$localWidgetSize';
  }
}
