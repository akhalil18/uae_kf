import 'dart:convert';
import 'dart:io';

import 'package:logger/logger.dart';

import '../core/preference/preference.dart';
import '../models/user.dart';
import '../services/auth_api.dart';

class AuthenticationService {
  User? _user;
  User? get user => _user;

  AuthApi api = AuthApi();

  AuthenticationService() {
    loadUser;
  }

  /*
   * Check activ code and add user to database
  */
  Future<void> checkActiveCode(
      {String? name,
      String? email,
      String? category,
      String? password,
      String? phone,
      String? gender,
      String? identity,
      String? deviceToken,
      File? image}) async {
    try {
      _user = await api.checkActiveCode(
          email: email!,
          password: password!,
          deviceToken: deviceToken!,
          gender: gender!,
          identity: identity!,
          image: image!,
          name: name!,
          category: category!,
          phone: phone!);
      if (_user != null) {
        // Logger().i(_user.toJson());
        saveUser(user: _user);
        saveUserToken(token: _user!.jwt);
      }
    } catch (e) {
      Logger().e(e);
    }
  }

  /*
  * Login user 
  */
  Future<void> login({String? email, String? password}) async {
    try {
      _user = await api.login(email!, password!);
      if (_user != null) {
        Logger().i(_user!.toJson());
        saveUser(user: _user);
        saveUserToken(token: _user!.jwt);
      }
    } catch (e) {
      Logger().e(e);
    }
  }

  /*
   * Update user profile info
   */
  Future<bool> updateProfile(
      {String? name,
      String? email,
      String? phone,
      String? gender,
      String? identity,
      File? image}) async {
    try {
      final updatedUser = await api.updateProfile(
          email: email!,
          gender: gender!,
          identity: identity!,
          image: image!,
          name: name!,
          phone: phone!);

      if (updatedUser != null) {
        print(updatedUser.toJson());
        Logger().i(updatedUser.toJson());
        _user = updatedUser;
        saveUser(user: _user);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }

  /*
   *check if user is authenticated 
   */
  bool get userLoged => Preference.getBool(PrefKeys.userLogged) ?? false;

  /*
   *save user in shared prefrences 
   */
  saveUser({User? user}) {
    Preference.setBool(PrefKeys.userLogged, true);
    Preference.setString(PrefKeys.userData, json.encode(user!.toJson()));
  }

  /*
   *save user token in shared prefrences 
   */
  saveUserToken({String? token}) {
    Preference.setString(PrefKeys.token, token!);
  }

  /*
   * load the user info from shared prefrence if existed to be used in the service   
   */
  Future<void> get loadUser async {
    if (userLoged) {
      _user =
          User.fromJson(json.decode(Preference.getString(PrefKeys.userData)!));
      Logger().i(_user!.toJson());
      print('\n\n\n\n');
    }
  }

  /*
   * signout the user from the app and return to the login screen   
   */
  Future<void> get signOut async {
    await Preference.remove(PrefKeys.userData);
    await Preference.remove(PrefKeys.userLogged);
    await Preference.remove(PrefKeys.token);

    _user = null;
  }
}
