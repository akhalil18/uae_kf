import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'core/localization/app_language.dart';
import 'core/localization/app_localizations.dart';
import 'core/preference/preference.dart';
import 'core/utils/provider_setup.dart';
import 'ui/screens/splash_screen/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preference.init();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppLanguage>(
      create: (_) => AppLanguage(),
      child: Consumer<AppLanguage>(
        builder: (context, appLanguage, child) => MultiProvider(
          providers: providers,
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'UAE KF',
            theme: ThemeData(
              primaryColor: Color(0xff1e284b),
              accentColor: Color(0xff57a96f),
              visualDensity: VisualDensity.adaptivePlatformDensity,
              fontFamily:
                  appLanguage.appLocal == Locale('en') ? 'Roboto' : "Cairo",
              // primaryColorLight: CC
            ),
            locale: appLanguage.appLocal,
            supportedLocales: [
              const Locale('en'),
              const Locale('ar'),
            ],
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            // onGenerateRoute: RouteGenerator.generateRoute,
            home: SplashScreen(),
          ),
        ),
      ),
    );
  }
}
