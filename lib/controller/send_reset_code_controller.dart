import '../core/utils/enums.dart';
import '../services/auth_api.dart';

class SendResetCodeController {
  Future<bool> sendCode(String email, ResetCode resetCodeType) async {
    final result = await AuthApi().sendCode(email, resetCodeType);
    return result!;
  }
}
