import '../services/volunteering_register_api.dart';

class VolunteeringRegisterController {
  Future<bool> registerInVolunteering({
    String? type,
    String? name,
    String? phone,
    String? hours,
    String? category,
    String? address,
  }) async {
    final statue = await VolunteeringRegisterApi().registerInVolunteering(
        type: type!,
        address: address!,
        category: category!,
        hours: hours!,
        name: name!,
        phone: phone!);

    return statue;
  }
}
