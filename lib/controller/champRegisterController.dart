import '../models/champ_registration.dart';
import '../services/champ_register_api.dart';

class ChampRegisterController {
  Future<bool> registerInChampionship(ChampRegistrationModel model) async {
    final statue = await ChampRegisterApi().registerInChamp(model);
    return statue!;
  }
}
