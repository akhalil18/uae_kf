import '../services/about_us_api.dart';

class AboutUsController {
  Future<String> getAboutUsDate() async {
    final aboutUstext = await AboutUsApi().getAboutUsData();
    return aboutUstext!;
  }
}
