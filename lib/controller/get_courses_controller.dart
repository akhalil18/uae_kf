import '../models/ModelCourse.dart';
import '../services/courses_api.dart';

class GetCoursesController {
  Future<List<Course>> getAllCourses() async {
    List<Course>? courses = await CoursesApi().getAllCourses();

    return courses!;
  }
}
