import '../services/auth_api.dart';

class ResetPasswordController {
  Future<bool> resetPassword(String email, String password) async {
    final result = await AuthApi().resetPassword(email, password);
    return result!;
  }
}
