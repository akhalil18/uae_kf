import 'dart:convert';

import 'package:UAE_KF/core/utils/AssetsRoutes.dart';
import 'package:UAE_KF/core/utils/GlobalVariables.dart';
import 'package:flutter/services.dart';

class StartUpController{

  initServerUrl() async {
    String serversList = await rootBundle.loadString(AssetsRoutes.servers);
    Map<String, dynamic> data = json.decode(serversList);

    GlobalVariables.baseUrl =
        data["Server"].toString();
  }
}