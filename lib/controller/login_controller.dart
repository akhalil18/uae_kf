import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../auth/authentication_service.dart';

class LogInController {
  Future<bool> login(
      {BuildContext? context, String? email, String? password}) async {
    final auth = Provider.of<AuthenticationService>(context!, listen: false);
    await auth.login(email: email, password: password);

    // if login fail
    if (auth.user == null) {
      return false;
    } else {
      // if login success
      return true;
    }
  }
}
