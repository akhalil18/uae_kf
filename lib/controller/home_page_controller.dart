import 'package:UAE_KF/models/ModelActivities.dart';

import '../models/home_page.dart';
import '../services/home_page_api.dart';

class HomePageController {
  Future<HomeData> getHomePageData() async {
    final homeData = await HomePageApi().getHomePage();
    return homeData!;
  }

  Future<ActivitiesData> getFederationActivities() async {
    final homeData = await HomePageApi().getFederationActivities();
    return homeData!;
  }

  Future<ActivitiesData> getInternationalAgenda() async {
    final homeData = await HomePageApi().getInternationalAgenda();
    return homeData!;
  }
}
