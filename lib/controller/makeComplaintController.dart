import '../services/make_complaint_api.dart';

class MakeComplaintController {
  Future<bool> makeComplaint({
    String? name,
    String? belt,
    String? agency,
    String? complaint,
    String? category,
  }) async {
    final statue = await MakeComplaintApi().makeComplaint(
        name: name!,
        agency: agency!,
        belt: belt!,
        complaint: complaint!,
        category: category!);

    return statue;
  }
}
