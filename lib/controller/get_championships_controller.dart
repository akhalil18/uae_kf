import '../models/modelChampionships.dart';
import '../services/getChampionshipsApi.dart';

class GetChampionshipsController {
  Future<List<Championship>> getallChampionships() async {
    final championships = await GetChampionshipsApi().getAllChampionships();
    return championships!;
  }
}
