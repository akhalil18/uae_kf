import '../services/contact_us_api.dart';

class ContactUsController {
  Future<bool> contactUs({String? email, String? message, String? name}) async {
    final statue = await ContactUsApi()
        .contactUs(email: email!, name: name!, message: message!);
    return statue;
  }
}
