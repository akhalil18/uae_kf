import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../auth/authentication_service.dart';

class UpdateProfileController {
  Future<bool> updatProfile({
    BuildContext? context,
    String? name,
    String? email,
    String? phone,
    String? gender,
    String? identity,
    File? image,
  }) async {
    final auth = Provider.of<AuthenticationService>(context!, listen: false);

    final result = await auth.updateProfile(
        name: name,
        email: email,
        phone: phone,
        gender: gender,
        identity: identity,
        image: image);

    return result;
  }
}
