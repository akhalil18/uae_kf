import '../services/training_register_api.dart';

class TrainingRegisterController {
  Future<bool> registerInTraining(
      {String? name, String? belt, String? agency}) async {
    final statue = await TrainingRegisterApi()
        .registerInTraining(agency: agency!, name: name!, belt: belt!);
    return statue;
  }
}
