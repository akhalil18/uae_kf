import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../auth/authentication_service.dart';
import '../services/auth_api.dart';

class SignUpController {
  Future<bool> checkActiveCode({
    BuildContext? context,
    String? name,
    String? category,
    String? email,
    String? password,
    String? phone,
    String? gender,
    String? identity,
    String? deviceToken,
    File? image,
  }) async {
    final auth = Provider.of<AuthenticationService>(context!, listen: false);
    await auth.checkActiveCode(
        name: name!,
        category: category!,
        email: email!,
        password: password!,
        phone: phone!,
        gender: gender!,
        identity: identity!,
        deviceToken: deviceToken!,
        image: image!);

    // if signUp fail
    if (auth.user == null) {
      return false;
    } else {
      // if signUp success
      return true;
    }
  }

  Future<int> signUp({
    String? name,
    String? category,
    String? email,
    String? password,
    String? phone,
    String? gender,
    String? identity,
    String? deviceToken,
  }) async {
    var code = await AuthApi().signUp(
      name: name!,
      category: category!,
      email: email!,
      password: password!,
      phone: phone!,
      gender: gender!,
      identity: identity!,
      deviceToken: deviceToken!,
    );
    return code!;
  }

  // Check if Email exist

  Future<bool> checkUserEmail(String email) async {
    final result = await AuthApi().checkUserEmail(email);
    return result!;
  }
}
