import '../services/auth_api.dart';

class CheckResetCodeController {
  Future<bool> checkResetCode(String email, String code) async {
    final result = await AuthApi().checkResetCode(email, code);
    return result!;
  }
}
