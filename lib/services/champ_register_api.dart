import 'dart:convert';

import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';
import '../models/champ_registration.dart';

class ChampRegisterApi {
  Future<bool?> registerInChamp(ChampRegistrationModel model) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.setUserChampionship;
      final Map<String, dynamic> body = {
        'championship_id': model.champId,
        'category': model.category,
        'championship_type': model.champType,
        'gender': model.gender
      };
      // print(
      //   '${model.champId} + ${model.category} + ${model.champName} + ${model.champType} + ${model.gender} +  ${model.weight}',
      // );

      if (model.champType == 'kumite') {
        body['weight'] = model.weight;
      }

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.userAuth,
      );

      final dataMap = json.decode(response.body) as Map<String, dynamic>;

      if (response.statusCode == 200) {
        return dataMap['status'];
      } else {
        return null;
      }
    } catch (e) {
      Logger().e(e);
      return null;
    }
  }
}
