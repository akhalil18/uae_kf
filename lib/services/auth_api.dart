import 'dart:convert';
import 'dart:io';

import 'package:UAE_KF/core/preference/preference.dart';
import 'package:UAE_KF/core/utils/enums.dart';
import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';
import '../models/user.dart';

class AuthApi {
  /*
   * Log user in
   */
  Future<User?> login(String email, String password) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.login;
      final body = {'email': email, 'password': password};

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: {"Content-Type": "application/json", 'lang': 'en'},
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final userResponse = UserResponse.fromJson(dataMap);

        return userResponse.user;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /*
   * create new user
   */
  Future<int?> signUp({
    String? name,
    String? category,
    String? email,
    String? password,
    String? phone,
    String? gender,
    String? identity,
    String? deviceToken,
  }) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.register;
      final Map<String, String> body = {
        'name': name!,
        'category': category!,
        'email': email!,
        'password': password!,
        'phone': phone!,
        'gender': gender!,
        'identity': identity!,
        'device_token': deviceToken!
      };

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: {"Content-Type": "application/json", 'lang': 'en'},
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        return dataMap['data']['code'];
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /*
   * Check if email exist
   */
  Future<bool?> checkUserEmail(String email) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.checkUserEmail;
      final Map<String, dynamic> body = {'email': email};

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.noAuth,
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;

        return dataMap['status'];
      } else {
        return null;
      }
    } catch (e) {
      Logger().e(e);
      return null;
    }
  }

  /*
   * Update user Info
   */
  Future<User?> updateProfile({
    String? name,
    String? email,
    String? phone,
    String? gender,
    String? identity,
    File? image,
  }) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.updateUserProfile;
      final request = http.MultipartRequest("POST", Uri.parse(url));

      print(gender);

      if (gender == 'ذكر') gender = 'male';
      if (gender == 'أنثي') gender = 'female';

      print(phone);

      final Map<String, String> body = {
        'name': name!,
        'email': email!,
        'gender': gender!,
        'identity': identity!
      };

      if (image != null) {
        var pic = await http.MultipartFile.fromPath("image", image.path);
        request.files.add(pic);
      }

      if (phone!.isNotEmpty) request.fields['phone'] = phone;
      request.fields.addAll(body);

      request.headers.addAll({
        "Content-Type": "application/json",
        'Authorization': 'Bearer ${Preference.getString(PrefKeys.token)}',
        'lang': 'en'
      });

      var streamResponse = await request.send();
      //Creates a new HTTP response from stream response
      var response = await http.Response.fromStream(streamResponse);

      final dataMap = json.decode(response.body) as Map<String, dynamic>;
      if (response.statusCode == 200) {
        final userResponse = UserResponse.fromJson(dataMap);
        return userResponse.user;
      } else {
        print(dataMap['message']);
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  /*
   * Send reset code to user email
   */
  Future<bool?> sendCode(String email, ResetCode resetCodeType) async {
    try {
      var path = ApiRoutes.forgetPassword;

      if (resetCodeType == ResetCode.resend) {
        path = ApiRoutes.resendResetCode;
      }

      var url = GlobalVariables.baseUrl! + path;
      final Map<String, dynamic> body = {'email': email};

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.noAuth,
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        return dataMap['status'];
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }

  /*
   * reset password
   */
  Future<bool?> resetPassword(String email, String password) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.resetPassword;
      final Map<String, dynamic> body = {'email': email, 'password': password};

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.noAuth,
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        return dataMap['status'];
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }

  /*
   * Check Reset Code
   */
  Future<bool?> checkResetCode(String email, String code) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.checkResetCode;
      final Map<String, dynamic> body = {'email': email, 'code': code};

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.noAuth,
      );

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        return dataMap['status'];
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }

  /*
   * Check activ code and add user to database
   */
  Future<User?> checkActiveCode({
    String? name,
    String? category,
    String? email,
    String? password,
    String? phone,
    String? gender,
    String? identity,
    String? deviceToken,
    File? image,
  }) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.checkActiveCode;
      final request = http.MultipartRequest("POST", Uri.parse(url));

      final Map<String, String> body = {
        'name': name!,
        'category': category!,
        'email': email!,
        'password': password!,
        'device_token': deviceToken!
      };

      if (image != null) {
        var pic = await http.MultipartFile.fromPath("image", image.path);
        request.files.add(pic);
      }

      request.fields.addAll(body);
      if (phone!.isNotEmpty) request.fields['phone'] = phone;
      if (gender!.isNotEmpty) request.fields['gender'] = gender;
      if (identity!.isNotEmpty) request.fields['identity'] = phone;

      request.headers
          .addAll({"Content-Type": "application/json", 'lang': 'en'});

      var streamResponse = await request.send();
      //Creates a new HTTP response from stream response
      var response = await http.Response.fromStream(streamResponse);

      print(response.statusCode.toString() + " - " + response.body);
      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final userResponse = UserResponse.fromJson(dataMap);

        return userResponse.user;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
