import 'dart:convert';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';
import '../models/ModelCourse.dart';

class CoursesApi {
  Future<List<Course>?> getAllCourses() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.getAllCourses;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final modelCourse = ModelCourse.fromJson(dataMap);

        return modelCourse.courses;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
