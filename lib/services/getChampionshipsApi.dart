import 'dart:convert';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';
import '../models/modelChampionships.dart';

class GetChampionshipsApi {
  Future<List<Championship>?> getAllChampionships() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.getAllChampionships;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final modelChampionship = ModelChampionships.fromJson(dataMap);

        return modelChampionship.championships;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
