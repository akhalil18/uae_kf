import 'dart:convert';

import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';

class MakeComplaintApi {
  Future<bool> makeComplaint({
    String? name,
    String? belt,
    String? agency,
    String? complaint,
    String? category,
  }) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.setUserComplaint;
      final Map<String, dynamic> body = {
        'name': name,
        'belt': belt,
        'agency': agency,
        'complaint': complaint,
        'category': category
      };

      var response = await http.post(
        Uri.parse(url),
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8")!,
        headers: Header.userAuth,
      );

      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }
}
