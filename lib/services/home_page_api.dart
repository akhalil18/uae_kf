import 'dart:convert';

import 'package:UAE_KF/models/ModelActivities.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';
import '../models/home_page.dart';

class HomePageApi {
  Future<HomeData?> getHomePage() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.homePage;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final homePageResponse = HomePageResponse.fromJson(dataMap);

        return homePageResponse.data;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<ActivitiesData?> getFederationActivities() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.getFederationActivities;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final modelFederationActivities = ModelActivities.fromJson(dataMap);

        return modelFederationActivities.data;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<ActivitiesData?> getInternationalAgenda() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.getInternationalAgenda;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;
        final modelInternationalAgenda = ModelActivities.fromJson(dataMap);

        return modelInternationalAgenda.data;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
