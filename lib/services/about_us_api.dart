import 'dart:convert';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import '../core/utils/header.dart';
import 'package:http/http.dart' as http;

class AboutUsApi {
  Future<String?> getAboutUsData() async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.getAboutUsPage;

      var response = await http.get(Uri.parse(url), headers: Header.noAuth);

      if (response.statusCode == 200) {
        final dataMap = json.decode(response.body) as Map<String, dynamic>;

        return dataMap['data'];
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
