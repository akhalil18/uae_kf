import 'dart:convert';

import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';

class TrainingRegisterApi {
  Future<bool> registerInTraining(
      {String? name, String? belt, String? agency}) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.setUserTraining;
      final Map<String, dynamic> body = {
        'name': name,
        'belt': belt,
        'agency': agency
      };

      var response = await http.post(
        Uri.parse(url),
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8")!,
        headers: Header.userAuth,
      );

      final dataMap = json.decode(response.body) as Map<String, dynamic>;

      if (response.statusCode == 200) {
        return dataMap['status'];
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }
}
