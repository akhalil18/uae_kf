import 'dart:convert';

import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import 'package:http/http.dart' as http;
import '../core/utils/header.dart';

class ContactUsApi {
  Future<bool> contactUs({String? name, String? message, String? email}) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.contactUs;
      final Map<String, dynamic> body = {
        'name': name,
        'email': email,
        'message': message
      };

      var response = await http.post(
        url,
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8"),
        headers: Header.noAuth,
      );

      if (response.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }
}
