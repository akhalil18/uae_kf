import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

import '../core/utils/ApiRoutes.dart';
import '../core/utils/GlobalVariables.dart';
import '../core/utils/header.dart';

class VolunteeringRegisterApi {
  Future<bool> registerInVolunteering({
    String? type,
    String? name,
    String? phone,
    String? hours,
    String? category,
    String? address,
  }) async {
    try {
      var url = GlobalVariables.baseUrl! + ApiRoutes.setUserVolunteering;
      final Map<String, dynamic> body = {
        'championship_type': type,
        'name': name,
        'phone': phone,
        'hours': hours,
        'category': category,
        'address': address
      };

      var response = await http.post(
        Uri.parse(url),
        body: jsonEncode(body),
        encoding: Encoding.getByName("utf8")!,
        headers: Header.userAuth,
      );

      final dataMap = json.decode(response.body) as Map<String, dynamic>;

      if (response.statusCode == 200) {
        return dataMap['status'];
      } else {
        return false;
      }
    } catch (e) {
      Logger().e(e);
      return false;
    }
  }
}
